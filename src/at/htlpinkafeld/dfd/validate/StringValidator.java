/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.dfd.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;


import org.apache.log4j.Logger;

/**
 *Pr�ft ob String OK ist
 *Gliederung form:Schule und andere Strings
 * @author Patrick
 */
@FacesValidator("validators.StringValidator")
public class StringValidator implements Validator {

	
	public static int swear_cnt = 0;
	private final static Logger l = Logger.getLogger(StringValidator.class);
	/**
	 * FORMAT SCHULE: 1. Buchstabe gro� danach restlichen klein
	 * FORMAT ANDERE: 1. Buchstabe oder eine Zahl, Rest: Buchstaben+Leerzeichen
	 */
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
    	l.debug("StringValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
    	String[] swearwords = {"gay","schwul","ficken", "fuck", "titten", "penis", "peidl", "nega", "muschi", "arsch", "bastard", "trottl", "idiot", "anal", "oral", "anus", "hurre", "nutte", "schlampe", "pisser", "mongo", "dildo", "heisel", "fotze", "kacke", "scheisse", "schei�e"};          
    	//Schimpfwortfilter
        String schule = uic.getClientId();
        String value = (String) o;
        if(!schule.equals("form:Schule")){
        if (!value.matches("[A-Z���][a-z����]+")) {
            //DFDUtilities.display("Falsches Format nur Buchstaben und der erste muss ein Gro�buchstabe sein", FacesMessage.SEVERITY_ERROR);
        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Falsches Format nur Buchstaben und der erste muss ein Gro�buchstabe sein","Falsches Format nur Buchstaben und der erste muss ein Gro�buchstabe sein"));
			
        }
        for(int i = 0; i < swearwords.length; i++) {
        	if(value.toLowerCase().contains(swearwords[i])) {
        		swear_cnt++;
        		switch(swear_cnt) {
        		case 1:
        			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Bitte keine Schimpfw�rter", "Bitte keine Schimpfw�rter"));	
        		case 2:
        			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Du Schlingel", "Du Schlingel"));	
        		case 3:
        			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Du Schlawiner", "Du Schlawiner"));	
        		default:
        			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Bitte keine Schimpfw�rter", "Bitte keine Schimpfw�rter"));	
        		}
            	
        }
        }
        }
        else{
            if (!value.matches("[A-Z0-9][A-Za-z���� ]+")) {
            //DFDUtilities.display("Falsches Format nur Buchstaben und Leerzeichen erlaubt und der erste Buchstabe muss gro� sein", FacesMessage.SEVERITY_ERROR);
            	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Falsches Format nur Buchstaben und Leerzeichen erlaubt und der erste Buchstabe muss gro� sein","Falsches Format nur Buchstaben und Leerzeichen erlaubt und der erste Buchstabe muss gro� sein"));
    			
            }
        }
        
    }

}
