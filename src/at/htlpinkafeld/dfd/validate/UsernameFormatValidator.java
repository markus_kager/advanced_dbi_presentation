package at.htlpinkafeld.dfd.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
/**
 * Pr�ft ob das Format des Benutzernames OK ist
 * @author Patrick
 *
 */
@FacesValidator("validator.UsernameFormatValidator")
public class UsernameFormatValidator implements Validator {

	private final static Logger l = Logger.getLogger(UsernameFormatValidator.class);
	/**
	 * Format: 1. Stelle Buchstabe danach Buchstaben,Zahlen oder _ m�glich und max. L�nge 30 Zeichen
	 */
	@Override
	public void validate(FacesContext fc, UIComponent uic, Object o)
			throws ValidatorException {
		l.debug("UsernameFormatValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
		if(o instanceof String){
			String str = (String)o;
			if(str.length() < 30){
				if(!str.matches("[A-Za-z]+[0-9_]*"))
					//DFDUtilities.display("Benutzername darf nur die Buchstaben A-Z,  a-z, die Ziffern 0-9 und _ enthalten", FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Benutzername darf nur die Buchstaben A-Z,  a-z, die Ziffern 0-9 und _ enthalten","Benutzername darf nur die Buchstaben A-Z,  a-z, die Ziffern 0-9 und _ enthalten"));
				
			}
			else
				 //DFDUtilities.display("Benutzername zu lang", FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Benutzername zu lang","Benutzername zu lang"));
			
		}
		else
			// DFDUtilities.display("Kein String wurde �bergeben", FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Kein String wurde �bergeben","Kein String wurde �bergeben"));
		
	}
	

}
