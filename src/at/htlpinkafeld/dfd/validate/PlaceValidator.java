/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.bean.DFDFacade;
import at.htlpinkafeld.dfd.pojo.Pair;
import at.htlpinkafeld.dfd.util.DFDUtilities;

/**
 *Pr�ft ab ob der Ort in der Ortedatenbank enthalten ist
 * @author Patrick
 */
@FacesValidator("validator.PlaceValidator")
public class PlaceValidator implements Validator{
    private final static Logger l = Logger.getLogger(PlaceValidator.class);
    private DFDFacade facade;
    public PlaceValidator() {
        try {
			facade = (DFDFacade)DFDUtilities.findBean("dfdFacade", DFDFacade.class);
		} catch (InstantiationException | IllegalAccessException e) {
			l.error("PlaceValidator - Konstruktor - InstantiationException | IllegalAccessException\n\n"+e.getMessage()+"\n\n");
				e.printStackTrace();
		}
    }
    /**
     * Pr�ft ab ob der Ort in der Datenbank enthalten ist
     * Note: Gro�- Kleinschreibung ist egal
     */
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {

        	l.debug("PlaceValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
            Pair place = (Pair)o;
            if(!facade.validPlace(place))
            	//DFDUtilities.display("Kein g�ltiger Ort", FacesMessage.SEVERITY_ERROR);
            	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Kein g�ltiger Ort","ein g�ltiger Ort"));
			
         
    }
    
    

   
    
    
}
