/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.htlpinkafeld.dfd.validate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.util.DFDUtilities;

/**
 *Pr�ft ab ob das Datum OK ist
 * @author Patrick
 */
@FacesValidator("validators.BirthdayValidator")
public class BirthdayValidator implements Validator {

	private final static Logger l = Logger.getLogger(BirthdayValidator.class);
    public BirthdayValidator() {
    }
    /**
     * Format: dd.MM.yyyy
     * erstes g�ltiges Datum 1.1.1960
     * letztes g�ltiges Datum das heutige
     */
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
    	l.debug("BirthdayValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
        String value = new SimpleDateFormat("dd.MM.yyyy").format((Date)o);
        if(!value.matches("[0-9]+[\\.][0-9]+[\\.][0-9]+")){
            //DFDUtilities.display("Falsches Format: 'dd.mm.yyyy'", FacesMessage.SEVERITY_ERROR);
        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Falsches Format: 'dd.mm.yyyy'","Falsches Format: 'dd.mm.yyyy'"));
        }
        String[] tab = value.split("\\.");
        int day = Integer.parseInt(tab[0]);
        int month = Integer.parseInt(tab[1]);
        int year = Integer.parseInt(tab[2]);
        if (!isYearOK(year) || !isMonthOK(month) || !isDayOK(day, month, year)) {
            //DFDUtilities.display("Ung�ltiges Datum", FacesMessage.SEVERITY_ERROR);
        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ung�ltiges Datum","Ung�ltiges Datum"));
        }

    }
    /**
     * 
     * @param y Jahr
     * @return true wenn das Jahr nicht kleiner als 1960 und gr��er als das derzeitige Datum
     */
    public boolean isYearOK(int y) {
        boolean ret = false;
        if (y >= 1960 && y <= Calendar.getInstance().get(Calendar.YEAR)) {
            ret = true;
        }
        return ret;
    }
    /**
     * 
     * @param m Monat
     * @return true wenn Monat zwischen 1-12
     */
    public boolean isMonthOK(int m) {
        boolean ret = false;
        if (m > 0 && m < 13) {
            ret = true;
        }
        return ret;
    }
    /**
     * Schaltjahr wird mit ein berechnet
     * @param d Tag
     * @param m Monat
     * @param y Jahr
     * @return true wenn Tag OK ist
     */
    public boolean isDayOK(int d, int m, int y) {
        boolean ret = false;
        int lastDay = 31;
        if (m == 4 || m == 6 || m == 9 || m == 11) {
            lastDay = 30;
        } else if (m == 2) {
            if (DFDUtilities.isLeapyear(y)) {
                lastDay = 29;
            } else {
                lastDay = 28;
            }
        }
        if (d > 0 && d <= lastDay) {
            ret = true;
        }
        return ret;
    }

}
