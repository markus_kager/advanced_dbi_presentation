package at.htlpinkafeld.dfd.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
/**
 * Pr�ft ab das Format des Passworts OK ist
 * @author Patrick
 *
 */
@FacesValidator("validator.PasswordFormatValidator")
public class PasswordFormatValidator implements Validator {
	private final static Logger l = Logger.getLogger(PasswordFormatValidator.class);
	/**
	 * Format: Buchstaben+Zahlen und nicht Gr��er als 30 Stellen
	 */
	@Override
	public void validate(FacesContext fc, UIComponent uic, Object o)
			throws ValidatorException {
		l.debug("PasswordFormatValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
		if(o instanceof String){
			String str = (String)o;
			if(str.length() > 30)
				//DFDUtilities.display("Zu langes Passwort", FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Zu langes Passwort","Zu langes Passwort"));
			
			else
			{
				if(!str.matches("[A-Za-z0-9]+")){
					//DFDUtilities.display("Es d�rfen nur Buchstaben A-Z und a-z und die Ziffern 0-9 eingegeben werden", FacesMessage.SEVERITY_ERROR);
					throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Es d�rfen nur Buchstaben A-Z und a-z und die Ziffern 0-9 eingegeben werden","Es d�rfen nur Buchstaben A-Z und a-z und die Ziffern 0-9 eingegeben werden"));
					
				}
			}
		}
		else
			//DFDUtilities.display("Kein String wurde �bergeben", FacesMessage.SEVERITY_ERROR);	
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Kein String wurde �bergeben",""));
		
	}

}
