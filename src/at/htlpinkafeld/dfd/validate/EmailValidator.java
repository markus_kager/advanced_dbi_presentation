package at.htlpinkafeld.dfd.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
/**
 * Pr�ft ab ob die Email OK ist
 * @author Patrick
 *
 */
@FacesValidator("validator.EmailValidator")
public class EmailValidator implements Validator{
	private final static Logger l = Logger.getLogger(EmailValidator.class);
	/**
	 * Format: Buchstaben+Zahlen optinal (.+weiter Zahlen+Buchstaben) @ Buchstaben[Gr��e 2-15] . Buchstaben[Gr��e 2-4] 
	 */
	@Override
	public void validate(FacesContext fc, UIComponent uic, Object o)
			throws ValidatorException {
		l.debug("EmailValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
		if(o instanceof String){
			String str = (String)o;
			if(!str.matches("[A-Za-z]+[A-Za-z0-9_\\.]*[\\.]?[A-Za-z0-9_\\.]+@[a-z]{2,15}\\.[a-z]{2,4}"))
				//DFDUtilities.display("Falsches Format", FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Falsches Format","Falsches Format"));
		}
		else
			//DFDUtilities.display("Kein String wurde �bergeben", FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Kein String wurde �bergeben","Kein String wurde �bergeben"));
			
	}

}
