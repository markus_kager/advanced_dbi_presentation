/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.RequiredValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

/**
 * Pr�ft ab ob die Klasse OK ist
 * @author Patrick
 */
@FacesValidator("validators.ClassValidator")
public class ClassValidator extends RequiredValidator implements Validator{

	private final static Logger l = Logger.getLogger(ClassValidator.class);
	/**
	 * Format: Zahl 0-9 und optinal Buchstaben
	 */
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
    	l.debug("ClassValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
        String msg = (String)o;
        if(!msg.matches("[0-9]+[a-zA-Z]*")){
        	 //DFDUtilities.display("Falsches Format: Zahl+beliebige Anzahl Buchstaben", FacesMessage.SEVERITY_ERROR);
        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Falsches Format: Zahl+beliebige Anzahl Buchstaben","Falsches Format: Zahl+beliebige Anzahl Buchstaben"));
        }
            
    }
    
    
    
    
    
}
