package at.htlpinkafeld.dfd.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.bean.DFDFacade;
import at.htlpinkafeld.dfd.pojo.User;
import at.htlpinkafeld.dfd.util.DFDUtilities;
/**
 * Pr�ft ob Username in der Datenbank schon enthalten ist
 * @author Patrick
 *
 */
@FacesValidator("validator.UsernameValidator")
public class UsernameValidator implements Validator {

	private final static Logger l = Logger.getLogger(UsernameValidator.class);
	private DFDFacade facade;
	public UsernameValidator() {
		try {
			facade = (DFDFacade)DFDUtilities.findBean("dfdFacade", DFDFacade.class);
		} catch (InstantiationException | IllegalAccessException e) {
			l.error("UsernameValidator - Konstruktor - InstantiationException | IllegalAccessException\n\n"+e.getMessage()+"\n\n");
				e.printStackTrace();
		}
	}
	/**
	 * Wirft Exception wenn Benutzername schon vergeben
	 */
	@Override
	public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
		l.debug("UsernameValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
		if(o instanceof String){
			if(!facade.validUsername(new User((String)o,"","")))
				//DFDUtilities.display("Benutzer existiert bereits", FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Benutzer existiert bereits","Benutzer existiert bereits"));
			
		}
		else
			//DFDUtilities.display("Kein String wurde �bergeben", FacesMessage.SEVERITY_ERROR);
		throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Kein String wurde �bergeben","Kein String wurde �bergeben"));
		
	}
	

}
