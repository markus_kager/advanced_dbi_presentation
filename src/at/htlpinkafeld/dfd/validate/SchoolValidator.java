/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.validate;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.bean.DFDFacade;
import at.htlpinkafeld.dfd.pojo.Pair;
import at.htlpinkafeld.dfd.util.DFDUtilities;

/**
 * Pr�ft ob die Schule OK ist und f�gt die Schule wenn es sie noch nicht gibt in die Datenbank ein
 * @author Patrick
 */
@FacesValidator("validator.SchoolValidator")
public class SchoolValidator implements Validator{
    private final static Logger l = Logger.getLogger(SchoolValidator.class);
    private DFDFacade facade;
    
    public SchoolValidator() {
        try {
			facade = (DFDFacade)DFDUtilities.findBean("dfdFacade", DFDFacade.class);
		} catch (InstantiationException | IllegalAccessException e) {
			l.error("SchoolValidator - Konstruktor - InstantiationException | IllegalAccessException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
    }
    
    
    /**
     * Format: Buchstaben optional[Leerzeichen+Buchstaben]
     */
    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        l.debug("SchoolValidator - validate(FacesContext fc, UIComponent uic, Object o): "+o);
    	Pair p = (Pair)o;
        if (!p.getValue().matches("[A-Z���][A-Z���a-z���� ]+")) {
        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Falsches Format nur Buchstaben und der erste muss ein Gro�buchstabe sein","Falsches Format nur Buchstaben und der erste muss ein Gro�buchstabe sein"));
        	//DFDUtilities.display("Falsches Format nur Buchstaben und der erste muss ein Gro�buchstabe sein", FacesMessage.SEVERITY_ERROR);
        }
        else
        {               
                if(!facade.generateKey(p))
                {
                    facade.insertSchule(p);
                    facade.generateKey(p);
                }         
        }
    }
    
 
    
}
