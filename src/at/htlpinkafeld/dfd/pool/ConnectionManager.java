package at.htlpinkafeld.dfd.pool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.database.dao.DAOException;
/**
 * Erzeugt einen ConnectionPool
 * Note: Connection Manager ist ein Singelton
 * @author Patrick
 *
 */
public class ConnectionManager{

	private static ConnectionManager INSTANCE;
	private DataSource dataSource;
	private Connection cn;
	private List<Connection> connections = new ArrayList<Connection>();
	private final static Logger l = Logger.getLogger(ConnectionManager.class);
    
	private ConnectionManager() {
		init();
	}
    private void init() {
    	l.debug("ConnectionManager - init()");
        try {
            // Get DataSource
            Context initContext  = new InitialContext();
            Context envContext  = (Context)initContext.lookup("java:/comp/env");
            dataSource = (DataSource)envContext.lookup("jdbc/Oracle");    
            //
        } catch (NamingException e) {
        	l.fatal("ConnectionManager - init() - NamingException\n\n"+e.getMessage()+"\n\n");
            e.printStackTrace();
        }
    }
    /**
     * 
     * @return eine neue Connection aus dem ConnectionPool
     * @throws DAOException DAOException
     */
    public synchronized Connection getNewConnection() throws DAOException{
    	try{cn = dataSource.getConnection();
    		connections.add(cn);
    	}catch(SQLException e){l.fatal("ConnectionManager - getNewConnection() - SQLException\n\n"+e.getMessage()+"\n\n");throw new DAOException(e);}
    	
    	return cn;
    }
    /**
     * 
     * @return die Instanz ConnectionManager
     */
    public static ConnectionManager newInstance(){
    	if(INSTANCE == null)
    		INSTANCE = new ConnectionManager();
    	l.debug("static ConnectionManager - newInstance()");
    	return INSTANCE;
    }

}
