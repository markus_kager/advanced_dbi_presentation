/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;



import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



import at.htlpinkafeld.dfd.util.DFDUtilities;

/**
 *
 * @author Markus
 */
public class StartBean implements Serializable{

	private static final long serialVersionUID = 7850290817105365522L;
	private final static Logger l = Logger.getLogger(StartBean.class);
	//zeigt bei Facade einen Fehler an, "wird nicht ben�tzt"
	//SuppressWarnings damit kein fehler angezeigt wird
    @SuppressWarnings("unused")
	private DFDFacade facade;
    private Locale loc ;
    private List<SelectItem> locations = new ArrayList<>();
    
    /**
     * Erzeugt eine neue Instanz von StartBean
     */
    public StartBean() {
    	initLanguages();
    	PropertyConfigurator.configureAndWatch("log4j.properties", 20*1000 );
          l.debug("StartBean-Konstruktor");        
          try {
			facade = (DFDFacade) DFDUtilities.findBean("dfdFacade", DFDFacade.class);
		} catch (InstantiationException | IllegalAccessException e) {
			l.fatal("StartBean - Konstruktor: Facade konnte nicht geladen werden\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
          //user = new User();      
    }
    /**
     * initialisiert die Sprachen: Deutsch und Englisch
     */
    private void initLanguages(){
    	loc = new Locale("de");
    	locations = new ArrayList<>();
    	locations.add(new SelectItem(new Locale("de"), "Deutsch"));
    	locations.add(new SelectItem(new Locale("en"), "Englisch"));
    }
    /*
    public User getUser() {
		return user;
	}
    public void setUser(User user) {
		this.user = user;
	}
	*/
    /**
     * 
     * @return input_mask.xhtml, wechselt auf input_mask.xhtml Seite
     */
    public String gotoInput_Mask(){
        return "input_mask.xhtml";
    }
    /**
     * 
     * @return output_list, wechselt auf die output_list.xhtml Seite
     */
    public String gotoOutput_List(){
        return "output_list";
    }
    /**
     * 
     * @return Login.xhtml, wechsel auf die Login.xhtml Seite
     */
    /*
    public String gotoLogin(){
        return "Login.xhtml";
    }
    /**
     * 
     * @return Registration.xhtml, wechsel auf die Registration.xhtml Seite
     */
    /*
    public String gotoRegistration(){
        return "Registration.xhtml";
    }
    */
    /**
     * 
     * @return Login.xhtml wenn er nicht scchon angemeldet ist sonst index.xhtml
     */
    /*
    public String gotoRecentPage(){
    	if(!user.isLogin())
    		return "Login.xhtml";
    	else
    		return "index.xhtml";
    }
    */
    /**
     * Einloggen des Users (user) in der Datenbank
     * @return null wenn Anmeldung nicht erfolgreich, index.xhtml wenn erfolgreich und wechsel auf die index.xhtml Seite
     */
    /*
    public String login(){
    	l.debug("StartBean-login()");
    	//if(loc == null)
    		//loc = new Locale("en");
    	String ret = null;
    	if(facade.login(user))
		{
    		user.setLogin(true);
			DFDUtilities.display("Wilkommen "+user.getName());
			ret = "index.xhtml";
		}
		else
		{
	    	DFDUtilities.display("falscher Username oder Passwort",FacesMessage.SEVERITY_ERROR);
		}
    	return ret;
	}
	*/
    /**
     * Ausloggen des Users
     * @return Login.xhtml, wechsel auf die Login.xhtml Seite
     * Wird in ADB herausgenommen, weil nicht wichtig und so
     */
    /*
    public String logout(){
    	l.debug("StartBean - logout()");
    	this.user = new User();  	
		DFDUtilities.display("Erfolgreich ausgeloggt");
    	return "Login.xhtml";
    }
    */
	public Locale getLoc() {
		return loc;
	}
	public void setLoc(Locale loc) {
		this.loc = loc;
	}
	public List<SelectItem> getLocations() {
		return locations;
	}
    
    
    
}
