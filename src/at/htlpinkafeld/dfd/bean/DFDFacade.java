package at.htlpinkafeld.dfd.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;



import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.database.DAOResult;
import at.htlpinkafeld.dfd.database.MetaData;
import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.database.dao.DAOException;
import at.htlpinkafeld.dfd.database.dao.DAOFactory;
import at.htlpinkafeld.dfd.database.dao.OracleDAO;
import at.htlpinkafeld.dfd.database.model.BenutzerModel;
import at.htlpinkafeld.dfd.database.model.Benutzer_PersonModel;
import at.htlpinkafeld.dfd.database.model.OrteModel;
import at.htlpinkafeld.dfd.database.model.PersonModel;
import at.htlpinkafeld.dfd.database.model.Person_ViewModel;
import at.htlpinkafeld.dfd.database.model.SchuleModel;
import at.htlpinkafeld.dfd.database.model.Schule_KlasseModel;
import at.htlpinkafeld.dfd.pojo.Benutzer_Person;
import at.htlpinkafeld.dfd.pojo.Pair;
import at.htlpinkafeld.dfd.pojo.Person;
import at.htlpinkafeld.dfd.pojo.Person_View;
import at.htlpinkafeld.dfd.pojo.Schule_Klasse;
import at.htlpinkafeld.dfd.pojo.User;
import at.htlpinkafeld.dfd.util.DFDUtilities;

public class DFDFacade implements Serializable{

	private static final long serialVersionUID = -1515538888812967795L;
	private DAO dao;
	private final static Logger l = Logger.getLogger(DFDFacade.class);
	/**
	 * Erzeugt eine Facade und ein DAO Objekt
	 */
	public DFDFacade() {
			open();
	}
	
	private void open(){
		l.debug("DAO Instanz wird erstellt");
		dao = DAOFactory.createDAO("oracle");
		l.debug("DAO Instanz ist erstellt worden");
	}
	
	/**
	 * 
	 * @param p speichert die Person p in die Datenbank
	 * @return true wenn speichern erfolgreich sonst false
	 */
	public boolean savePerson(Person p){
		boolean ret = false;
		PersonModel personModel = new PersonModel(p,1);
		PersonModel insertPersonModel = new PersonModel(p);
		Person_ViewModel person_viewModel = new Person_ViewModel(new Person_View(p,p.getSchule_klasse(),p.getOrt(),p.getSchule_klasse().getSchule()),1);
		
            try{ 
            	DAOResult find = dao.find(personModel.getTablename(), personModel.getColumns(), personModel.getData(), personModel.getTyp(),OracleDAO.getAndList(personModel.getColumnCount()-1) );  
            	
            	if(!find.isErg()){
	            dao.insert(insertPersonModel.getTablename(), insertPersonModel.getColumns(), insertPersonModel.getData(), insertPersonModel.getTyp());

	             DAOResult r = dao.find(person_viewModel.getTablename(), person_viewModel.getColumns(), person_viewModel.getData(), person_viewModel.getTyp() , OracleDAO.getAndList(person_viewModel.getColumnCount()-1));
	            
	            p = ((Person_View)r.getMetaData().get(0).generateElement(person_viewModel)).getP();
	            ret = true;
            }
            } catch (DAOException ex) {
	        	l.error("DFDFacade - saveInDatabase(Person p): DAOException\n\n"+ex.getMessage()+"\n\n");
	            ex.printStackTrace();
        	} catch (Exception e) {
        		l.error("DFDFacade - saveInDatabase(Person p): Exception\n\n"+e.getMessage()+"\n\n");
				e.printStackTrace();
			}
            return ret;
		
	}
	/**
	 * 
	 * @param number max. Gr��e der Liste aus Personen
	 * @return	die Personenliste der zuletzt hinzugef�gten Personen
	 */
	public List<Person> getLastInsertedPeople(int number){
		List<Person> ret = null;
    	Person_ViewModel person_viewModel = new Person_ViewModel(new Person_View());
    	try {
    		DAOResult r = dao.topNAnalyse(person_viewModel.getTablename(), number, DAO.KLEINER_GLEICH, person_viewModel.getColumns(),"ID", DAO.DESC);
    			if(r != null){
    				List<MetaData> l = r.getMetaData();
    				ret = new ArrayList<>();
    				for(MetaData data:l){
    					ret.add(((Person_View)(data.generateElement(person_viewModel))).getP());
    				}
    			}
			else
				ret = new ArrayList<>();
		} catch (DAOException e) {
			l.error("DFDFacade - lastInsertedPeople(int number): DAOException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		} catch (Exception e1) {
			l.error("DFDFacade - lastInsertedPeople(int number): Exception\n\n"+e1.getMessage()+"\n\n");
			ret = new ArrayList<>();
			e1.printStackTrace();
		}
    	return ret;
	}
	/**
	 * 
	 * @param updatePerson	Die zu updatende Person
	 * @param currentPerson die zu ersetzende Person
	 * @return	true wenn update erfolgreich
	 */
	public boolean updatePerson(Person updatePerson, Person currentPerson){
		boolean ret = false;
		try{
		if(updatePerson.getSchule_klasse().getSchule().getKey() != currentPerson.getSchule_klasse().getSchule().getKey() || !updatePerson.getSchule_klasse().getKlasse().equals(currentPerson.getSchule_klasse().getKlasse()))
			generateKey(updatePerson.getSchule_klasse());
		PersonModel personModel = new PersonModel(updatePerson,1);
    	int index = 1;
    	DAOResult find = dao.find(personModel.getTablename(), personModel.getColumns(), personModel.getData(), personModel.getTyp(), OracleDAO.getAndList(personModel.getColumnCount()-1));
    	if(!find.isErg()){
    		personModel = new PersonModel(updatePerson); 	
    	if(!updatePerson.getVname().equals(currentPerson.getVname())){
    		ret = true;
    		dao.update(personModel.getTablename(), personModel.getColumns()[index], personModel.getData()[index], personModel.getColumns()[0], currentPerson.getId()+"",personModel.getTyp()[index],personModel.getTyp()[0]);
    	}
    	index++;
    	if(!updatePerson.getNname().equals(currentPerson.getNname())){
    		ret = true;
    		dao.update(personModel.getTablename(), personModel.getColumns()[index], personModel.getData()[index], personModel.getColumns()[0], currentPerson.getId()+"",personModel.getTyp()[index],personModel.getTyp()[0]);
    	}
    	index++;
    	if(!updatePerson.getOrt().equals(currentPerson.getOrt())){
    		ret = true;
    		dao.update(personModel.getTablename(), personModel.getColumns()[index], personModel.getData()[index], personModel.getColumns()[0], currentPerson.getId()+"",personModel.getTyp()[index],personModel.getTyp()[0]);
    	}
    	index++;
    	if(!Person.FORMAT.format(updatePerson.getGebDate()).equals(Person.FORMAT.format(currentPerson.getGebDate()))){
    		ret = true;
    		dao.update(personModel.getTablename(), personModel.getColumns()[index], personModel.getData()[index], personModel.getColumns()[0], currentPerson.getId()+"",personModel.getTyp()[index],personModel.getTyp()[0]);
    	}
    	index++;
    	if(updatePerson.isGeschlecht()!=(currentPerson.isGeschlecht())){
    		ret = true;
    		dao.update(personModel.getTablename(), personModel.getColumns()[index], personModel.getData()[index], personModel.getColumns()[0], currentPerson.getId()+"",personModel.getTyp()[index],personModel.getTyp()[0]);
    	}
    	index++;
    	if(!updatePerson.getSchule_klasse().equals(currentPerson.getSchule_klasse())){
    		ret = true;
    		dao.update(personModel.getTablename(), personModel.getColumns()[index], personModel.getData()[index], personModel.getColumns()[0], currentPerson.getId()+"", personModel.getTyp()[index], personModel.getTyp()[0]);
    	}
    	
    	}
    	else
    		DFDUtilities.display("Person existiert bereits!");
		}catch(DAOException e){
			l.error("DFDFacade - updatePerson(Person updatePerson, Person currentPerson); - Fehler beom updaten\n\n"+e.getMessage()+"\n\n");
			ret = false;
			e.printStackTrace();
		}
		
		return ret;
	}

	/**
	 * 
	 * @param u der User der die Person erstellt hat
	 * @param p die zu l�schende Person
	 * @return true wenn Person gel�scht wurde
	 */
	public boolean deletePerson(User u, Person p){
		boolean ret = false;
		try{
			Benutzer_PersonModel benutzer_personModel = new Benutzer_PersonModel(new Benutzer_Person(u, p));
	    	dao.delete(benutzer_personModel.getTablename(), benutzer_personModel.getColumns(), benutzer_personModel.getData(), benutzer_personModel.getTyp(),OracleDAO.getAndList(1));
	    	PersonModel personModel = new PersonModel(p,1);
	    	dao.delete(personModel.getTablename(), personModel.getColumns(), personModel.getData(), personModel.getTyp(),OracleDAO.getAndList(personModel.getColumnCount()-1));
	    	ret = true;
		}catch(DAOException e){
			l.error("DFDFacade - deletePerson(User u, Person p) - Fehler beim L�schen der Person\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * 
	 * @param u der User der aktuellen Session
	 * @param p die Person f�r die geschaut wird ob die Button freigegeben werden
	 * @return true wenn der User das Recht hat die Person zu �ndern oder zu l�schen
	 */
	public boolean showButton(User u, Person p){
		boolean ret = false;
		Benutzer_PersonModel benutzer_personModel = new Benutzer_PersonModel(new Benutzer_Person(u, p));
    	try {
    		DAOResult r = dao.identicalFind(benutzer_personModel.getTablename(), benutzer_personModel.getColumns(), benutzer_personModel.getData(), benutzer_personModel.getTyp(), OracleDAO.getAndList(benutzer_personModel.getColumns().length-1));
			if(r.isErg())
				ret = true;
    	} catch (DAOException  e) {
    		l.error("DFDFacade - showButton(User u, Person p): DAOException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		} catch (Exception e) {
			l.error("DFDFacade - showButton(User u, Person p): Exception\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
		return ret;
	}
	/**
	 * 
	 * @param str der Suchbegriff
	 * @return List der gefunden Personen
	 */
	public List<Person> search(String str, int sortSelect){
		List<Person> ret = null;
		Set<Person> s = new TreeSet<>();
		Person_ViewModel person_viewModel = new Person_ViewModel(new Person_View(), new int[]{1,2,4,9,10});
        
            for(String data:person_viewModel.getColumns())
            	try {
            	DAOResult set = dao.search(person_viewModel.getTablename(), data, str,DAO.PROZENT_LINKS_RECHTS, sortSelect);
            		List<MetaData> l = set.getMetaData();
            		for(MetaData d: l)
            			s.add(((Person_View)d.generateElement(person_viewModel)).getP());
            ret = new ArrayList<>(s);
        } catch (DAOException ex) {
        	l.error("DFDFacade - search(String str) - Fehler beim Suchen von Personen\n\n"+ex.getMessage()+"\n\n");
        	ex.printStackTrace();
        } catch (Exception e) {
        	l.error("DFDFacade - search(String str) - Exception\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
		return ret;
	}
	
	public List<Person> searchAll(String str){
		List<Person> ret = null;
		Set<Person> s = new TreeSet<>();
		Person_ViewModel person_viewModel = new Person_ViewModel(new Person_View(), new int[]{1,2,4,9,10});
        
            for(String data:person_viewModel.getColumns())
            	try {
            	DAOResult set = dao.search(person_viewModel.getTablename(), data, "",DAO.PROZENT_LINKS_RECHTS, 0);
            		List<MetaData> l = set.getMetaData();
            		for(MetaData d: l)
            			s.add(((Person_View)d.generateElement(person_viewModel)).getP());
            ret = new ArrayList<>(s);
        } catch (DAOException ex) {
        	l.error("DFDFacade - search(String str) - Fehler beim Suchen von Personen\n\n"+ex.getMessage()+"\n\n");
        	ex.printStackTrace();
        } catch (Exception e) {
        	l.error("DFDFacade - search(String str) - Exception\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
		return ret;
	}
	/**
	 * 
	 * @param str der Suchbegriff
	 * @return List der gefunden Orte
	 */
	public List<Pair> searchForOrte(String str){
		List<Pair> ret = new ArrayList<>();
		OrteModel m = new OrteModel(new Pair(0, str));
		try {
			DAOResult r = dao.search(m.getTablename(), m.getColumns()[1], str, DAO.PROZENT_RECHTS, 0);
			List<MetaData> l = r.getMetaData();
    		for(MetaData d: l)
    			ret.add((Pair)d.generateElement(m));
		} catch (DAOException ex) {
			l.error("DFDFacade - searchForOrte(String str) - Fehler beim Suchen von Orten\n\n"+ex.getMessage()+"\n\n");
			ex.printStackTrace();
		} catch (Exception ex) {
			l.error("DFDFacade - searchForOrte(String str) Exception - Fehler beim Suchen von Orten\n\n"+ex.getMessage()+"\n\n");
			ex.printStackTrace();
		}
		return ret;
	}
	/**
	 * 
	 * @param u User der in der Datenbank gespeichert wird
	 * @return true wenn Speicherung erfolgreich
	 */
	public boolean registrateUser(User u){
		boolean ret = false;
		BenutzerModel benutzer_insertDAO = new BenutzerModel(u);
		try {
			dao.insert(benutzer_insertDAO.getTablename(), benutzer_insertDAO.getData(), benutzer_insertDAO.getTyp());
			ret = true;
		} catch (DAOException e) {
			l.error("DFDFacade - registrate(User u): DAOException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
		return ret;
	}
	/**
	 * 
	 * @param u User der sich einloggen will
	 * @return true wenn der User in der Datenbank vorhanden und das Passwort korrekt ist
	 */
	public boolean login(User u){
		boolean ret = false;
		u.setEmail("");
		BenutzerModel benutzer_personModel = new BenutzerModel(u,0,2);
		try {
			DAOResult r = dao.identicalFind(benutzer_personModel.getTablename(), benutzer_personModel.getColumns(), benutzer_personModel.getData(), benutzer_personModel.getTyp(), OracleDAO.getAndList(benutzer_personModel.getColumnCount()-1));
		
			if(r.isErg())
				ret = true;
		} catch (DAOException e) {
			l.error("DFDFacade - login(User u): DAOException\n\n"+e.getMessage()+"\n\n");		
			e.printStackTrace();
		} 
		return ret;
	}

	/**
	 * 
	 * @param place Der Orte nach dem gesucht wird ob er g�ltig ist
	 * @return true wenn es den Ort gibt false wenn nicht
	 * 
	 * Note: wenn der Ort gefundne wird wird auch die ID als key beim Pair Object hinzugef�gt falls nicht bleibt der key unver�ndert
	 */
	public boolean validPlace(Pair place){
		boolean ret = false;
		OrteModel orteModel = new OrteModel(place,1);
		try{DAOResult r = dao.find(orteModel.getTablename(), orteModel.getColumns(), orteModel.getData(),orteModel.getTyp(),OracleDAO.getAndList(orteModel.getColumnCount()-1));
            if(r.isErg())
            {
            	place.setKey(((Pair)r.getMetaData().get(0).generateElement(orteModel)).getKey());
                ret = true;
            }
        } catch (DAOException ex) {
        	l.error("DFDFacade - validPlace(Pair place): DAOException\n\n"+ex.getMessage()+"\n\n");
        	ex.printStackTrace();          
		} catch (Exception ex) {
			l.error("DFDFacade - validPlace(Pair place): Exception\n\n"+ex.getMessage()+"\n\n");
			ex.printStackTrace();
		}
		return ret;
	}
	/**
	 * 
	 * @param u der Benutzer der �berpr�t wird
	 * @return true wenn es den Benutzer gibt false wenn nicht
	 */
	public boolean validUsername(User u){
		boolean ret = false;
		BenutzerModel benutzerModel = new BenutzerModel(u,0,1);
		try {
			DAOResult result = dao.identicalFind(benutzerModel.getTablename(), benutzerModel.getColumns(), benutzerModel.getData(), benutzerModel.getTyp(),OracleDAO.getAndList(benutzerModel.getColumnCount()-1));
			if(!result.isErg())
				ret = true;
		} catch (DAOException e) {
			l.error("DFDFacade - validUsername(User u): DAOException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		} 
		return ret;
	}
	
	/**
	 * 
	 * @param schule die Schule nach der gesucht wird
	 * @return true wenn es die Schule gibt, false wenn nicht
	 * 
	 * Note: wenn die Schule gefunden wird wird das key Attribut auf die ID der Schule gesetzt!
	 */
	public boolean generateKey(Pair schule){
		boolean ret = false;
		l.debug("SchoolValidator - validate(FacesContext fc, UIComponent uic, Object o) - generateKey(Pair p): "+schule);
		SchuleModel schuleModel = new SchuleModel(schule,1);
		try {
			DAOResult result = dao.find(schuleModel.getTablename(), schuleModel.getColumns(), schuleModel.getData(),schuleModel.getTyp(),OracleDAO.getAndList(schuleModel.getColumnCount()-1));
		
        if(result.isErg())
        {
            schule.setKey((Integer)result.getMetaData().get(0).getElement(0).getElement());
            ret = true;
        }
		} catch (DAOException e) {
			l.error("DFDFacade - generateKey(Pair schule) - DAOException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
            
		return ret;
	}
	/**
	 * 
	 * @param p die einzuf�gende Schule
	 * @return true wenn Speichern erfolgreich
	 */
	public boolean insertSchule(Pair p){
		boolean ret = false;
		SchuleModel schuleModel = new SchuleModel(p);
		try {
			dao.insert(schuleModel.getTablename(), schuleModel.getColumns(), schuleModel.getData(),schuleModel.getTyp());
		} catch (DAOException e) {
			l.error("DFDFacade - insertSchule(Pair p) - DAOException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
		return ret;
	}
	
	/**
	 * 
	 * @param schule_klasse das Schule_Klasse Object das gesucht wird
	 * @return true wenn es in der Datenbank vorhanden ist, false wenn nicht
	 * 
	 * Note:wenn gefundne wird das 'key' Attribut auf die ID der Schule_Klasse gesetzt!
	 */
	public boolean generateKey(Schule_Klasse schule_klasse){
		boolean ret = false;
		Schule_KlasseModel schule_klasseModel = new Schule_KlasseModel(schule_klasse,1);
		
		try {
			DAOResult result = dao.find(schule_klasseModel.getTablename(), schule_klasseModel.getColumns(), schule_klasseModel.getData(), schule_klasseModel.getTyp(),OracleDAO.getAndList(schule_klasseModel.getColumnCount()-1));
			if(result.isErg())
			{
					schule_klasse.setKey((Integer)result.getMetaData().get(0).getElement(0).getElement());
				ret = true;
			}
		} catch (DAOException e) {
			l.error("DFDFacade - generateKey(Schule_Klasse schule_klasse) - DAOException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		} 
		return ret;
	}
	/**
	 * 
	 * @param schule_klasse das einzuf�gende Schule_Klasse Element
	 * @return true wenn Speichern erfolgreich
	 */
	public boolean insertSchule_Klasse(Schule_Klasse schule_klasse){
		boolean ret = false;
		Schule_KlasseModel schule_klasseModel = new Schule_KlasseModel(schule_klasse);
		try {
			dao.insert(schule_klasseModel.getTablename(), schule_klasseModel.getColumns(), schule_klasseModel.getData(), schule_klasseModel.getTyp());
		} catch (DAOException e) {
			l.error("DFDFacade - insertSchule_Klasse(Schule_Klasse schule_klasse) - DAOException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
		return ret;
	}
	
}
