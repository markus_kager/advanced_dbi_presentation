/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.bean;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import at.htlpinkafeld.dfd.pojo.Person;
import at.htlpinkafeld.dfd.util.DFDUtilities;

/**
 *
 * @author Markus
 */
public class PersonenBean implements Serializable{

	private final static Logger l = Logger.getLogger(PersonenBean.class);
	private static final long serialVersionUID = -3908593844321121587L;
	private String search;
    private List<Person> personen = new ArrayList<>();
    private Person p;
    private Person update_p;
    private List<SelectItem> genderList = new ArrayList<>();
    private List<SelectItem> sortList = new ArrayList<>();
    private int sortSelect = 1;
    private DFDFacade facade;
    /**
     * Erzeugt eine neue Instanz von PersonenBean
     */
    public PersonenBean() {
        try {
			facade = (DFDFacade) DFDUtilities.findBean("dfdFacade", DFDFacade.class);
		} catch (InstantiationException | IllegalAccessException e) {
			l.fatal("PersonenBean - Konstruktor: Facade konnte nicht geladen werden\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
        personen = facade.searchAll(search);
        sortList.add(new SelectItem(1, "Name"));
        sortList.add(new SelectItem(2, "Schule"));
        sortList.add(new SelectItem(3, "Wohnort"));
    }

    /**
     * 	Sucht nach den Personen die search enthalten
        Note: Suchen von Ortschaft und Schule und klasse M�glich
        @return null da die Seite neu geladen werden muss
    */
    public String searchForResults(){
        personen = facade.search(search, this.sortSelect);      
        /*FacesMessage mess = new FacesMessage(FacesMessage.SEVERITY_INFO,"SQL-Befehl", "SQL-Befehl");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, mess);*/
        sort();
        return null;
    }
    
    public String searchForAllResults() {
    	personen = facade.searchAll(search);
    	sort();
    	return null;
    }
    
    public void sort() {
    	Collections.sort(personen, new Comparator<Person>() {
    		public int compare(Person p, Person p2) {
    			int solution = 0;
    			
    			switch(sortSelect) {
	    			case 1:
	    				solution = p.getVname().compareTo(p2.getVname());
	    				
	    				if(solution == 0) {
	    					solution = p.getNname().compareTo(p2.getNname());
	    				}
	    				break;
	    			case 2:
	    				solution = p.getSchule_klasse().getSchule().getValue().compareTo(p2.getSchule_klasse().getSchule().getValue());
	    				break;
	    			case 3:
	    				solution = p.getOrt().compareTo(p2.getOrt());
	    				break;
	    				
    			}
    			
    			return solution;
    		}
    	});
    }

    public List<Person> getPersonen() {
        return personen;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getSortSelect() {
    	return this.sortSelect;
    }
    
    public void setSortSelect(int i) {
    	this.sortSelect = i;
    }
    
    public List<SelectItem> getSortList() {
		return sortList;
	}
    
    public void setSortList(List<SelectItem> sortList) {
		this.sortList = sortList;
	}
    
    public Person getP() {
        return p;
    }

    public void setP(Person p) {
        this.p = p;
    }
    public String booleanToGeschlecht(){
    	if(p != null)
    		return FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "var").getString(DFDUtilities.booleanAsString(p.isGeschlecht()));
    	else
    		return null;
    }
    /**
     * baua geh weg
     * �ndern die Person p aus der Datenbank mit update_p
     * 
     * @return null da die Seite neu geladen werden muss
     * @throws SQLException Exception
     */
    public String updatePerson() throws SQLException{
    	l.debug("PersonenBean - updatePerson()");
    	Iterator<FacesMessage> i = FacesContext.getCurrentInstance().getMessages();
    	if(!i.hasNext()){
    	//System.out.println("updatePerson(nicht updaten)");
    	if(facade.updatePerson(update_p,p)){
    		DFDUtilities.display("Person erfolgreich geupdatet");
    		l.debug("PersonenBean - updatePerson() - Person ergolgreich upgedatet: "+update_p);
    	
	    	personen = DFDUtilities.setElementInList(personen, p, update_p);
	    	this.p = new Person(update_p);
	
	    	RequestContext.getCurrentInstance().update("form:table");
	    	RequestContext.getCurrentInstance().execute("PF('dlgUpdate').hide();");
    	}else
    	{  		
    		DFDUtilities.display("Person wurde nicht ver�ndert");
    		RequestContext.getCurrentInstance().update("form:table");
        	RequestContext.getCurrentInstance().execute("PF('dlgUpdate').hide();");
        	l.debug("PersonenBean - updatePerson() - Person wurde nicht ver�ndert");
    	}
    	}
    		return null;
    }
        
    public Person getUpdate_p() {
		return update_p;
	}
    public void setUpdate_p(Person update_p) {
		this.update_p = new Person(update_p);
	}
    /**
     * �ffnet den Update-Dialog
     * 
     * @param person die neue Person
     */
    public void gotoUpdatePage(Person person){
    	l.debug("PersonenBean - gotoUpdatePage(Person person): "+person);
    	this.p = person;
    	update_p = new Person(person);
    	ResourceBundle b = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "var");
    	genderList.clear();
    	genderList.add(new SelectItem(update_p.isGeschlecht(),b.getString(DFDUtilities.booleanAsString(update_p.isGeschlecht()))));
    	genderList.add(new SelectItem(!update_p.isGeschlecht(),b.getString(DFDUtilities.booleanAsString(!update_p.isGeschlecht()))));
    	RequestContext.getCurrentInstance().update("dialog_update");
    	RequestContext.getCurrentInstance().execute("PF('dlgUpdate').show();");
    }
    /**
     * 
     * @param person die zu l�schende Person
     * @return null da die Seite neu geladen werden muss
     * @throws SQLException Exception
     */
    public String deletePerson(Person person) throws SQLException{
    	l.debug("PersonenBean - deletePerson(Person person): "+person);
    	

	    	personen.remove(person);
	    	DFDUtilities.display("Person erfolgreich gel�scht");
	    	l.debug("PersonenBean - deletePerson(Person person) - Person erfolgreich gel�scht" );
    	/*
    	else
    	{
    		DFDUtilities.display("Person konnte nicht gel�scht werden");
        	l.error("PersonenBean - deletePerson(Person person) - Person konnte nicht gel�scht werden" );
    	}
    	*/
    	return null;
    }
    /**
     * �ffnet einen Dialog wo die Person angezeigt wird
     * @param person die anzuzeigende Person
     */
    public void gotoOutputPage(Person person){
    	l.debug("PersonenBean - gotoOutputPage(Person person): "+person);
    	this.p = person;
    	RequestContext.getCurrentInstance().update("dialog");
    	RequestContext.getCurrentInstance().execute("PF('dlg').show();");  	
    }

    public List<SelectItem> getGenderList() {
		return genderList;
	}
    public void setGenderList(List<SelectItem> genderList) {
		this.genderList = genderList;
	}
    /**
     * Pr�ft ab ob der aktuelle User das Recht hat die Person zu �ndern oder zu l�schen
     * @param p die Person auf die zugegriffen werden will
     * @return true wenn der User das Recht zum l�schen und �ndern hat
     */
    public boolean showButton(Person p){   	
    	boolean ret = false;
    		ret = true;
    	l.debug("PersonenBean - showButton(Person p): "+p+" Return: "+ret);
    	return ret;
    }
    
    public void update_info() throws SQLException{
    	l.debug("PersonenBean - updatePerson()");
    	Iterator<FacesMessage> i = FacesContext.getCurrentInstance().getMessages();
    	if(!i.hasNext()){
    	//System.out.println("updatePerson(nicht updaten)");
    	if(facade.updatePerson(update_p,p)){
	    	personen = DFDUtilities.setElementInList(personen, p, update_p);
	    	StringBuilder sb = new StringBuilder();
	    	String s;
	    	
	    	switch(comparePerson(p,update_p)){
	    	case "Vorname" 	  : sb.append("update user").append(" set ").append("vorname = ").append(update_p.getVname()).append(" where vorname = ").append(p.getVname());
	    						s = sb.toString();
	    						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, s, " "));
	    						break;
	    	case "Nachname"   : sb.append("update user").append(" set ").append("nachname = ").append(update_p.getNname()).append(" where nachname = ").append(p.getVname());
	    						s = sb.toString();
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, s, " "));
								break;
	    	case "Ort"        : sb.append("update user").append(" set ").append("ort = ").append(update_p.getOrt()).append(" where Ort = ").append(p.getVname());
								s = sb.toString();
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, s, " "));
								break;
	    	case "GebDat"     : sb.append("update user").append(" set ").append("Geburtsdatum = ").append(update_p.getGebDate()).append(" where Geburtsdatum = ").append(p.getVname());
								s = sb.toString();
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, s, " "));
								break;
	    	case "Geschlecht" : sb.append("update user").append(" set ").append("Geschlecht = ").append(update_p.isGeschlecht()).append(" where Geschlecht = ").append(p.getVname());
								s = sb.toString();
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, s, " "));
								break;
	    	case "Schule"     : sb.append("update user").append(" set ").append("Schule = ").append(update_p.getSchule_klasse().getSchule()).append(" where Schule = ").append(p.getVname());
								s = sb.toString();
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, s, " "));
								break;
	    	case "Klasse"     : sb.append("update user").append(" set ").append("Klasse = ").append(update_p.getSchule_klasse().getKlasse()).append(" where Klasse = ").append(p.getVname());
								s = sb.toString();
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, s, " "));
								break;
	    	case "nichts"          : 
								s = "Nichts wurde ver�ndert";
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, s, " "));
								break;
	    	}
    	}
    }    	
}
    
    public String comparePerson(Person pers,Person p){
    	if(!(pers.getVname().equals(p.getVname()))){
    		return "Vorname";
    	}
    	if(!(pers.getNname().equals(p.getNname()))){
    		return "Nachname";
    	}
    	if(!(pers.getOrt().equals(p.getOrt()))){
    		return "Ort";
    	}
    	if(!(pers.getGebDate().equals(p.getGebDate()))){
    		return "GebDat";
    	}
    	if(!(pers.isGeschlecht() == p.isGeschlecht())){
    		return "Geschlecht";
    	}
    	if(!(pers.getSchule_klasse().getSchule().equals(p.getSchule_klasse().getSchule()))){
    		return "Schule";
    	}
    	if(!(pers.getSchule_klasse().getKlasse().equals(p.getSchule_klasse().getKlasse()))){
    		return "Klasse";
    	}
    	
    	return "nichts";
    	
    }
}
