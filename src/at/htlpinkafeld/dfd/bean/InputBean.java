/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

import at.htlpinkafeld.dfd.pojo.DefaultSize;
import at.htlpinkafeld.dfd.pojo.Pair;
import at.htlpinkafeld.dfd.pojo.Person;
import at.htlpinkafeld.dfd.util.DFDUtilities;


/**
 *
 * @author Hannes
 */
public class InputBean implements Serializable{

	private static final long serialVersionUID = 4709397713412730920L;
	private static final Logger l = Logger.getLogger(InputBean.class);
    private Person p;
    private DefaultSize size;
    private DFDFacade facade;
    private List<SelectItem> geschlecht_item = new ArrayList<>();
    private List<Person> lastPeople;
    
    /**
     * Erzeugt ein InputBean Element
     */
    public InputBean() {
    	size = new DefaultSize();
       try {
		facade = (DFDFacade) DFDUtilities.findBean("dfdFacade", DFDFacade.class);
       } catch (InstantiationException | IllegalAccessException e) {
    	   l.fatal("InputBean - Konstruktor: Facade konnte nicht geladen werden\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
       p = new Person();
       ResourceBundle b = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "var");
       geschlecht_item.add(new SelectItem(false, b.getString("male")));
       geschlecht_item.add(new SelectItem(true, b.getString("female")));
       lastPeople = facade.getLastInsertedPeople(5);
    }

    public Person getP() {
        return p;
    }
    /**
     * 
     * @return Liste der zuletzt hinzugef�gten Personen
     */
    public List<Person> getLastPeople() {
		return lastPeople;
	}
    public void setLastPeople(List<Person> lastPeople) {
		this.lastPeople = lastPeople;
	}

    public void setP(Person p) {
        this.p = p;
    }
    /**
     * 
     * @return Liste f�r die Kombobox Geschlecht
     */
    public List<SelectItem> getGeschlecht_item() {
		return geschlecht_item;
	}
    
    /**
     * Speichert Person in der Datenbank ab und den User der die Person erstellt hat
     * @return null damit die Seite noch einmal aufgerufen wird
     */
    public String saveInDatabase() {
    	l.debug("InputBean - saveInDatabase()");
    	if(!FacesContext.getCurrentInstance().getMessages().hasNext()){
    	if(!facade.generateKey(p.getSchule_klasse()))
    	{
    		facade.insertSchule_Klasse(p.getSchule_klasse());
    		facade.generateKey(p.getSchule_klasse());
    	}
    	if(facade.savePerson(p)){
            lastPeople = DFDUtilities.addPersonOnTop(lastPeople, p,5);
            p = new Person();   
            RequestContext.getCurrentInstance().execute("showDBProcess()");
            DFDUtilities.display("Person wird gespeichert",FacesMessage.SEVERITY_INFO);
	    }
        else
                DFDUtilities.display("",FacesMessage.SEVERITY_ERROR);
    	} 
        return null;
    }
    

    /**
     *  return Person.toString();  */
    @Override
    public String toString() {       
        return p.toString();
    }
    
    public List<SelectItem> findMatches(String str){
    	List<SelectItem> ret = new ArrayList<>();
    	for(Pair p: facade.searchForOrte(str)){
    		ret.add(new SelectItem(p.getValue(), p.getValue()));
    	}
    	return ret;
    }
    public DefaultSize getSize() {
		return size;
	}public void setSize(DefaultSize size) {
		this.size = size;
	}
    
}
