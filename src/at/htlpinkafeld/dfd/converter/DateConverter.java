/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.converter;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.pojo.Person;

/**
 *Wandelt String in Date Format(dd.MM.yyyy) um und wieder zur�ck
 * @author Patrick
 */
@FacesConverter("converter.DateConverter")
public class DateConverter extends DateTimeConverter{

	private final static Logger l = Logger.getLogger(DateConverter.class);
	/**
	 * Wandelt String in Date Format(dd.MM.yyyy)
	 */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
    	l.debug("DateConverter - getAsObject(FacesContext fc, UIComponent uic, String value): "+value);
        Object o = null;
        setPattern("dd.MM.yyyy");
        try{
            o = super.getAsObject(context, component, value); //To change body of generated methods, choose Tools | Templates.
        }catch(ConverterException ex){
        	l.debug("DateConverter - getAsObject(FacesContext fc, UIComponent uic, String value) - Falsches Format: TT.MM.JJJJ");
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Falsches Format: TT.MM.JJJJ","Falsches Format: TT.MM.JJJJ"));
        }
        return o;
    }
    /**
     * Wandelt Date zur�ck in einen String um. String FORMAT in Person.FORMAT abgespeichert
     */
    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object o) {
    	l.debug("DateConverter - getAsString(FacesContext fc, UIComponent uic, Object o): "+o.toString());
    	return Person.FORMAT.format((Date)o);
    }
}
