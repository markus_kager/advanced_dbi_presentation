package at.htlpinkafeld.dfd.converter;

import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
/**
 * Wandelt einen String in ein Locale Objekt um
 * @author Patrick
 *
 */
@FacesConverter("converter.LocaleConverter")
public class LocaleConverter implements Converter {

	/**
	 * Wandelt str in ein new Locale(str) um
	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String str) {
		return new Locale(str);
	}
	/**
	 * Gibt o.toString(); zur�ck.
	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object o) {
		return o.toString();
	}

}
