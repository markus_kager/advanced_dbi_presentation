/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;


/**
 * Wandelt String in String um :)
 * @author Patrick
 */
@FacesConverter("converter.ClassConverter")
public class ClassConverter implements Converter{

	private final static Logger l = Logger.getLogger(ClassConverter.class);
	
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
    	l.debug("ClassConverter - getAsObject(FacesContext fc, UIComponent uic, String string): "+string);
        return string;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    	l.debug("ClassConverter - getAsString(FacesContext fc, UIComponent uic, Object o): "+o.toString());
        return o.toString();
    }
    
}
