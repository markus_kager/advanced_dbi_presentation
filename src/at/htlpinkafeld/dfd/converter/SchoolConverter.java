/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.pojo.Pair;

	/**
	 *Wandelt String in Pair um und wieder zur�ck
	 * @author Patrick
	 */
@FacesConverter("converter.SchoolValidator")
public class SchoolConverter implements Converter{

	private final static Logger l = Logger.getLogger(SchoolConverter.class);
	/**
	 * Wandelt String in Pair um
	 */
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
    	l.debug("SchoolConverter - getAsObject(FacesContext fc, UIComponent uic, String string): "+string);
        return new Pair(0, string);
    }
	/**
	 * Wandelt Pair in String um. return = (Pair)o.getValue();
	 */
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    	l.debug("SchoolConverter - getAsString(FacesContext fc, UIComponent uic, Object o): "+o.toString());
    	if(o instanceof Pair)
        return ((Pair)o).getValue();
    	else
    		return o.toString();
    }
    
}
