package at.htlpinkafeld.dfd.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
/**
 * Util Klasse des DFD Projekts
 * @author Patrick
 *
 */
public class DFDUtilities {

	private final static Logger l = Logger.getLogger(DFDUtilities.class);
	/**
	 * 
	 * @param l liste die gek�rzt werden muss
	 * @param off ab welchen index die Liste anfangen soll
	 * @return die neue Liste
	 */
	public static Object[] getSubList(Object[] l, int off) {
		return getSubList(l, off, l.length);
	}
	/**
	 * 
	 * @param l liste die gek�rzt werden muss
	 * @param off ab welchen index die Liste anfangen soll
	 * @param set bis zu diesem Index soll die Liste gek�rzt werden
	 * @return die neue Liste
	 */
	public static Object[] getSubList(Object[] l, int off, int set) {
		Object[] ret = null;
		if (off > -1 && set > -1) {
			if (off < set) 
				if(set <= l.length){
					ret = new Object[set - off];
					int c = 0;
					for (int i = off; i < set; i++) {
						ret[c] = l[i];
						c++;
				}
			}
		}
		return ret;
	}
	/**
	 * 
	 * @param l Liste
	 * @return die Integerliste
	 */
	public static Integer[] getIntegerListOfObject(Object[] l) {
		Integer[] ret = new Integer[l.length];
		for (int i = 0; i < ret.length; i++)
			ret[i] = (Integer) l[i];
		return ret;
	}
	/**
	 * 
	 * @param l Liste
	 * @return die Integerliste
	 */
	public static String[] getStringList(Object[] l) {
		String[] ret = new String[l.length];
		for (int i = 0; i < ret.length; i++)
			ret[i] = (String) l[i];
		return ret;
	}
	/**
	 * 
	 * @param l die int Tabelle
	 * @return int[] als Integer[]
	 */
	public static Integer[] getIntegerList(int[] l) {
		Integer[] ret = new Integer[l.length];
		for (int i = 0; i < ret.length; i++)
			ret[i] = l[i];
		return ret;
	}
	/**
	 * 
	 * @param l Integer[]
	 * @return die Integer[] als int[]
	 */
	public static int[] getIntList(Integer[] l) {
		int[] ret = new int[l.length];
		for (int i = 0; i < ret.length; i++)
			ret[i] = l[i];
		return ret;
	}
	
	/**
	 * 
	 * @param b geschlecht
	 * @return Weiblich bei true, M�nnlich bei false
	 */
	public static String booleanAsString(boolean b) {
		if (b)
			return "female";
		else
			return "male";
	}
	/**
	 * 
	 * @param b boolean
	 * @return return 1 bei true, 0 bei false
	 */
	public static String booleanToString(boolean b){
    	l.debug("PersonInsertDAO - booleanToString(boolean b): "+b);
        if(b)
            return "1";
        else
            return "0";
    }
	/**
	 * 
	 * @param b boolean
	 * @return das gegenteil von b
	 */
	public static boolean getBooleanOpposit(boolean b) {
		return !b;
	}
	/**
	 * 
	 * @param y year
	 * @return true wenn Jahr ein Schaltjahr ist
	 */
	public static boolean isLeapyear(int y) {
		boolean ret = false;
		if (((y % 4) == 0 && (y % 100) == 0 && (y % 400) == 0)
				|| ((y % 4) == 0 && (y % 100) != 0 && (y % 400) != 0)) {
			ret = true;
		}
		return ret;
	}
	
	/**
	 * Zeigt die Message str mit Severity INFO an
	 * @param str String
	 */
	public static void display(String str) {
		l.debug("displayMessage(String str): "+str);
		display(str, FacesMessage.SEVERITY_INFO);
	}
	/**
	 * Zeigt die Nachricht str mit der Severity severity an
	 * @param str String
	 * @param severity Severity
	 */
	public static void display(String str, FacesMessage.Severity severity) {
		l.debug("displayMessage(String str, FacesMessage.Severity severity): "+str+" , "+severity);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, str, ""));
	}
	
    /**
     * @param <T>		Der Typ der Liste
     * @param l liste
     * @param pos der letzte Index der Liste
     * @return die neue Liste
     */
    public static<T> List<T> cutList(List<T> l, int pos){
    	return l.subList(0, pos+1);
    }
    /**
     * @param <T>		Der Typ der Liste 
     * @param l liste
     * @return die Liste nur von hinten nach vorne
     */
    public static<T> List<T> reverseList(List<T> l){
    	List<T> ret = null;
    	if(!l.isEmpty()){
    		ret = new ArrayList<T>();
    		
	    	for(int i = l.size()-1; i >= 0; i--)
	    		ret.add(l.get(i));
	    }
    	return ret;
    	
    }
    /**
     * 
     * @param i i
     * @return  1 = true, rest = false
     */
    public static boolean intToBoolean(int i){
    	if(l != null)
    	l.debug("PersonenBean - intToBoolean(int i): "+i);
        if(i == 1)
            return true;
        else
            return false;
    }
    /**
     * 
     * @param name						Name der Bean in der FacesConfig oder als Annotation
     * @param beanClass					Klasse der Bean(muss die Klasse sein von dem Namen der Bean)
     * @return							Die gesuchte Bean
     * @throws InstantiationException	Falls die Bean nicht geladen werden konnte
     * @throws IllegalAccessException	Falls die Bean nicht geladen werden konnte
     * 
     * Die Bean wird wenn sie angefordert wird gesucht. Wenn sie nicht existiert wird sie erzeugt und dann eine Refenece von dem Objekt zur�ckgegeben.
     * Note: Die Bean existiert nur 1. Mal.
     * Note: Es muss nach einer SessionBean gesucht werden sonst wird sie nicht gefunden
     */
    public static Object findBean(String name,Class<?> beanClass) throws InstantiationException, IllegalAccessException{
    	Object ret = null;
    	Map<String, Object>beans = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    	ret = beans.get(name);
    	if(ret == null)
    	{
    		ret = beanClass.newInstance();
    		beans.put(name, ret);
    	}
    	
    	return ret;
    }
    
    /**
     * @param <T>		Der Typ des Arrays 
     * @param l Die Liste in die eingef�gt wird
     * @param element Das Element das in die Liste eingef�gt wird
     * @param length ab welcher Gr��e abgeschnitten wird
     * @return Die ver�nderte Liste
     * 
     * F�gt das Element ganz oben in die Liste ein und entfernt das letzte Element der Liste, so bliebt die Gr��e der Liste gleich
     */
    public static<T> List<T> addPersonOnTop(List<T> l,T element,int length){
    	List<T> ret = null;
    	if(l != null){
    		int size = l.size();
    		ret = l;
    		if(size < 1){
    			ret.clear();
    			ret.add(element);
    		}
    		else{
    			Collections.reverse(ret); 
                ret.add(element);
                if(l.size() > length)
                ret = DFDUtilities.cutList(DFDUtilities.reverseList(ret), length-1);
                else
                	ret = DFDUtilities.reverseList(ret);
    		}
    	}
    	return ret;
    }
    /**
     * @param <T>			Der Typ des Arrays
     * @param l				Liste aller Elemente
     * @param oldElement    Das Elelemt das ersetzt werden soll
     * @param newElement	Das zuersetzende Element
     * @return				Die Liste alle Elemente
     * 
     * Note: Bei den Elementen die �bergeben werden muss die equals Methode �berschrieben werden sonst funktioniert die Funktion nicht!
     */
    public static<T> List<T> setElementInList(List<T> l,T oldElement,T newElement){
    	List<T> ret = null;
    	if(l != null){
    		ret = l;
    		for(int i = 0; i < ret.size(); i++){
    			if(ret.get(i).equals(oldElement))
    				ret.set(i, newElement);
    		}
    	}
    	return ret;
    }
    /**
     * @param <T>		Der Typ des Arrays
     * @param l			Die Ausgangsliste
     * @param indizes	Die Indize die aus der Ausgangsliste in die neue �bernommen werden
     * @return			Die List nur mit den Indizes-Werten
     */
    public static<T> Object[] separateArray(Object[] l, int[] indizes){
    	Object[] ret = null;
    	List<Object> array = null;
    	if(l != null)
    		if(indizes.length>0){
    			array = new ArrayList<>();
    			for(int k = 0; k < l.length; k++){
    				if(DFDUtilities.containsInt(indizes, k))
    					array.add(l[k]);
    			}
	    		ret = new Object[indizes.length];
	    		for(int i = 0; i < ret.length; i++)
	    			ret[i] = array.get(i);
    	}
    	return ret;
    }
    /**
     * 
     * @param array the Array
     * @param value the contaning value
     * @return true wenn value ist in array enthalten
     */
    public static boolean containsInt(int[] array,int value){
    	boolean ret = false;
    	for(int e:array){
    		if(e == value)
    			ret = true;
    	}
    	return ret;
    }
    /**
     * 
     * @param str der zu verschl�sselnde String
     * @return der String verschl�sselt
     */
    public static String decryptString(String str){
    	String ret = null;
    	try{
			//Holen einer MessageDigest Instanz
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			//Passwort wird in einen Hash umgewandelt
			byte[]digest = md.digest(str.getBytes());
			
			//Hash dem hashpwd zuweisen
			ret = "";
			for(byte b:digest){
				ret += String.format("%02x",b);
			}
		}catch(NoSuchAlgorithmException e){
			l.error("DFDUtilities - decryptString(String str) - NoSuchAlgorithmException\n\n"+e.getMessage()+"\n\n");
			e.printStackTrace();
		}
    	return ret;
    }
    
    @Deprecated
    public static String encryptString(String str){
    	String ret = null;
    	try{
			//Holen einer MessageDigest Instanz
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			//Passwort wird in einen Hash umgewandelt
			byte[]digest = md.digest(str.getBytes());
			
			//Hash dem hashpwd zuweisen
			
			for(byte b:digest){
				ret += String.format("%02x",b);
			}
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace();
		}
    	return ret;
    }
    
    public static String ArrayToString(Object[] array){
    	return Arrays.toString(array);
    }
    public static String ArrayToString(int[] array){
    	return Arrays.toString(array);
    }
}
