package at.htlpinkafeld.dfd.database;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.database.model.Model;
/**
 * Das java.sql.ResultSet wird in ein MetaData Objekt umgewandelt
 * 
 * MetaData = eine Zeile aus dem ResultSet
 * @author Patrick
 *
 */
public class MetaData implements Serializable {

	private static final long serialVersionUID = -4438499324483948761L;
	/**
	 * index im ResultSet
	 */
	private List<Integer> index;
	/**
	 * Wert im ResulSet
	 */
	private List<Value<?>> value;
	/**
	 * Typ des Wertes
	 */
	private List<Integer> type;
	
	public MetaData() {
		index = new ArrayList<>();
		value = new ArrayList<>();
		type = new ArrayList<>();
	}
	/**
	 * 
	 * @param index   index im ResultSet
	 * @param element der Wert an der Stelle index im ResultSet
	 * @param type Typ des Wertes an der Stelle index im ResultSet
	 */
	public void add(int index,Value<?> element,int type){
		this.index.add(index);
		this.value.add(element);
		this.type.add(type);
	}
	
	public int getType(int index){
		return type.get(index);
	}
	
	public int getIndex(int index){
		return this.index.get(index);
	}
	
	public Value<?> getElement(int index){
		return value.get(index);
	}
	/**
	 * 
	 * @param model das Datenbanktabellenmodel
	 * @return Das Element
	 * @throws InstantiationException Exception
	 * @throws IllegalAccessException Exception
	 * @throws NoSuchFieldException Exception
	 * @throws SecurityException Exception
	 * @throws NoSuchMethodException Exception
	 * @throws IllegalArgumentException Exception
	 * @throws InvocationTargetException Exception
	 */
	public Object generateElement(Model<?> model) throws InstantiationException, IllegalAccessException, NoSuchFieldException, SecurityException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException{
		Object ret = null;
		Class<?> c = model.getElement().getClass();
		Class<?>[] paratyps = new Class[type.size()];
		Object[] val = new Object[value.size()];
		for(int i=0; i < type.size();i++){
			if(type.get(i) == DAO.NUMBER){
			paratyps[i] = int.class;
			val[i] = (Integer)value.get(i).getElement();
			}
			else if(type.get(i) == DAO.STRING){
				paratyps[i] = String.class;
				val[i] = (String)value.get(i).getElement();
			}
			else if(type.get(i) == DAO.DATE){
				paratyps[i] = Date.class;
				val[i] = (Date)value.get(i).getElement();
				}
		}
		Constructor<?> con = c.getConstructor(paratyps);
		ret = con.newInstance(val);

		return ret;
	}

}
