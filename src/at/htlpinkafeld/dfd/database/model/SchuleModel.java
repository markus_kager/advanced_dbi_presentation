/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.database.model;

import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.pojo.Pair;

/**
 * Das Datenbanktabellenmodel f�r die Tabelle Schule
 * @author Patrick
 *
 */
public class SchuleModel extends AbstractModel<Pair>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5483265327386938283L;
	protected static int MAXLENGTH = 2;

	public SchuleModel(Pair element, int off, int set) {
		super(element, off, set, MAXLENGTH);
	}

	public SchuleModel(Pair element, int set) {
		super(element, set, MAXLENGTH);
	}

	public SchuleModel(Pair element) {
		super(element, MAXLENGTH);
	}
	public SchuleModel(Pair element, int[] columns) {
		super(element, columns, MAXLENGTH);
	}
	
	@Override
	protected String[] initColumns(){
		l.debug("SchuleModelDAO - initColumns()");
    	String[] ret = new String[MAXLENGTH];
    	ret[0] = "id";
    	ret[1] = "schulname";
    	return ret;
    }
	@Override
    protected String[] initData(){
		l.debug("SchuleModelDAO - initData()");
		l.debug("Schule_KlasseInsertDAO - initData()");
    	String[] ret = new String[MAXLENGTH];
        ret[0] = pks+".nextval";
        ret[1] = element.getValue();
        return ret;
    }
    @Override
    protected int[] initTyp(){
    	l.debug("SchuleModelDAO - getTyp()");
   	 	int[] ret = new int[MAXLENGTH];
        for(int i = 0; i < ret.length; i++)
        {
            if(i != 0)
                ret[i] = DAO.STRING;
            else
                ret[i] = DAO.NUMBER;
        }
        return ret;
   }
    @Override
    protected String initTablename() {
    	l.debug("SchuleModelDAO - initTablename()");
    	return "Schule";
    }
    @Override
    protected String initPKS() {
    	l.debug("SchuleModelDAO - initPKS()");
    	return "SCHULE_PKS";
    }
    

    
    
    
   
    
}
