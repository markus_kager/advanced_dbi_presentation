package at.htlpinkafeld.dfd.database.model;

import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.pojo.Person_View;
import at.htlpinkafeld.dfd.util.DFDUtilities;
/**
 * Das Datenbanktabellenmodel f�r die View Person_View
 * @author Patrick
 *
 */
public class Person_ViewModel extends AbstractModel<Person_View> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3505956971972752070L;
	private static final int MAXLENGTH = 11;
	
	public Person_ViewModel(Person_View element, int off, int set) {
		super(element, off, set, MAXLENGTH);
	}

	public Person_ViewModel(Person_View element, int off) {
		super(element, off, MAXLENGTH);
	}

	public Person_ViewModel(Person_View element) {
		super(element, MAXLENGTH);
	}
	public Person_ViewModel(Person_View element, int[] columns) {
		super(element, columns, MAXLENGTH);
	}
	

	@Override
	protected String[] initColumns() {
		String[] ret = new String[MAXLENGTH];
		ret[0] = "id";
		ret[1] = "vorname";
		ret[2] = "nachname";
		ret[3] = "ort";
		ret[4] = "ortsname";
		ret[5] = "geburtsdatum";
		ret[6] = "geschlecht";
		ret[7] = "schule";
		ret[8] = "schul_id";
		ret[9] = "schulname";
		ret[10] = "klasse";
		return ret;
	}

	@Override
	protected String[] initData() {
		String[] ret = new String[MAXLENGTH];
		ret[0] = element.getP().getId()+"";
		ret[1] = element.getP().getVname();
		ret[2] = element.getP().getNname();
		ret[3] = element.getOrt().getKey()+"";
		ret[4] = element.getOrt().getValue();
		ret[5] = element.getP().getFormatedGebDate();
		ret[6] = DFDUtilities.booleanToString(element.getP().isGeschlecht());
		ret[7] = element.getSchule_klasse().getKey()+"";
		ret[8] = element.getSchule().getKey()+"";
		ret[9] = element.getSchule().getValue();
		ret[10] = element.getSchule_klasse().getKlasse();
		return ret;
	}

	@Override
	protected int[] initTyp() {
		int[] ret = new int[MAXLENGTH];
		for(int i = 0; i < ret.length; i++){
			if(i == 5)
				ret[i] = DAO.DATE;
			else
				if(i == 0 || i == 3 || i == 6 || i == 7 || i == 8)
					ret[i] = DAO.NUMBER;
				else
					ret[i] = DAO.STRING;
		}
		return ret;
	}

	@Override
	protected String initTablename() {
		return "Person_View";
	}

	@Override
	protected String initPKS() {
		return null;
	}

}
