/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.database.model;

import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.pojo.Person;
import at.htlpinkafeld.dfd.util.DFDUtilities;

/**
 * Das Datenbanktabellenmodel f�r die Tabelle Person
 * @author Patrick
 *
 */
public class PersonModel extends AbstractModel<Person>{
    
	private static final long serialVersionUID = -3529330671521513832L;
	private static int MAXLENGTH = 7;

    public PersonModel(Person element, int off, int set) {
		super(element, off, set, MAXLENGTH);
	}


	public PersonModel(Person element, int off) {
		super(element, off, MAXLENGTH);
	}


	public PersonModel(Person element) {
		super(element, MAXLENGTH);
	}
	
	public PersonModel(Person element, int[] columns) {
		super(element, columns, MAXLENGTH);
	}

	@Override
    public String[] initColumns() {
    	l.debug("PersonInsertDAO - initColumns()");
    	String[] ret = new String[MAXLENGTH];
    	ret[0] = "id";
    	ret[1] = "vorname";
    	ret[2] = "nachname";
    	ret[3] = "ort";
    	ret[4] = "geburtsdatum";
    	ret[5] = "geschlecht";
    	ret[6] = "schule";
        return ret;
    }

   
    @Override
    public String[] initData() {
    	l.debug("PersonInsertDAO - initData()");
        String[] ret = new String[MAXLENGTH];
        ret[0] = pks+".nextval";
        ret[1] = element.getVname();
        ret[2] = element.getNname();
        ret[3] = element.getOrt().getKey()+"";
        ret[4] = element.getFormatedGebDate();
        ret[5] = DFDUtilities.booleanToString(element.isGeschlecht());
        ret[6] = element.getSchule_klasse().getKey()+"";
        return ret;
    }
    
    

    @Override
    public int[] initTyp() {
    	l.debug("PersonInsertDAO - initTyp()");
        int[] ret = new int[MAXLENGTH];
        for(int i = 0; i < ret.length; i++){
            if(i != 5 && i != 0 && i != 3 && i !=6){
                if(i == 4)
                    ret[i] = DAO.DATE;
                else    
                    ret[i] = DAO.STRING;
            }
            else
                ret[i] = DAO.NUMBER;
        }
        return ret;
    }
    
    @Override
	public String initTablename() {
    	l.debug("PersonInsertDAO - initTablename()");
		return "Person";
	}
    @Override
    public String initPKS() {
    	l.debug("PersonInsertDAO - initPKS()");
    	return "PERSON_PKS";
    }
    
}
