package at.htlpinkafeld.dfd.database.model;

import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.util.DFDUtilities;
/**
 * 
 * @author Patrick
 *
 * @param <T> Type
 */
public abstract class AbstractModel<T> implements Model<T> {


	private static final long serialVersionUID = 2630170688553479893L;
	protected static Logger l = Logger.getLogger(AbstractModel.class);
	private int maxLength = 0;
	protected String tablename = null;
	protected String[] columns = null;
    protected String[] data = null;
    protected int[] typ = null;
	protected String pks = null;
    protected T element = null;
    /**
     * 
     * @param element das Element aus dem die Daten generiert werden
     * @param maxLength die max. L�nge der Tabellen
     */
    public AbstractModel(T element,int maxLength) {
		this(element,0,maxLength);
	}
    /**
     * 
     * @param element das Element aus dem die Daten generiert werden
     * @param off der Startindex
     * @param maxLength die max. L�nge der Tabellen
     */
    public AbstractModel(T element,int off,int maxLength) {
		this(element,off,maxLength,maxLength);		
   	}
    /**
     * 
     * @param element das Element aus dem die Daten generiert werden
     * @param off der Startindex
     * @param set der Grenzindex
     * @param maxLength die max. L�nge der Tabellen
     */
    public AbstractModel(T element,int off, int set,int maxLength) {
    	init(element,maxLength);
        cutArrays(off, set);
   	}
    /**
     * 
     * @param element das Element aus dem die Daten generiert werden
     * @param columns die Indizes die verwendet werden von den gesamten Daten
     * @param maxLength die max. L�nge der Tabellen
     * 
     * Note: die restlichen Werte werden verworfen
     */
    public AbstractModel(T element, int[] columns,int maxLength){
    	init(element, maxLength);
    	separateArrays(columns);
    }
    /**
     * Daten werden initialisiert die initMethoden werden aufgerufen
     * @param element 
     * @param maxLength
     */
    private void init(T element,int maxLength){
    	l.debug("AbstractModel - init(T element,int maxLength): "+element+" , "+maxLength);
    	this.element = element;
    	this.maxLength = maxLength;
    	tablename = initTablename();
    	pks = initPKS();
        columns = initColumns();
        data = initData();
        typ = initTyp();
    }
	/**
	 * 
	 * @return die Spalten der Tabelle
	 */
    protected abstract String[] initColumns();
    /**
     * 
     * @return die Werte als String der Tabelle
     */
    protected abstract String[] initData();
    /**
     * 
     * @return die Typen der Tabellenspalten
     */
    protected abstract int[] initTyp();
    /**
     * 
     * @return den Tabellenname
     */
    protected abstract String initTablename();
    /**
     * 
     * @return null wenn keine Sequenz verwendet wird, sonst den Namen der Sequenz
     */
    protected abstract String initPKS();
    
    /**
     * die Tabellen werden ab off bis set gek�rzt
     * @param off 
     * @param set
     */
    protected void cutArrays(int off,int set){
    	l.debug("AbstractDAO - cutArrays(int off,int set):"+off+" , "+set);
    	if(off >= 0 || set < maxLength){
    	columns = DFDUtilities.getStringList(DFDUtilities.getSubList(columns, off, set));
    	data = DFDUtilities.getStringList(DFDUtilities.getSubList(data, off, set));
    	typ = DFDUtilities.getIntList(DFDUtilities.getIntegerListOfObject(DFDUtilities.getSubList(DFDUtilities.getIntegerList(typ), off, set)));
    	}
    }
    /**
     * die Tabellen Indizes in indizes werden nur mehr verwendet
     * @param indizes
     */
    protected void separateArrays(int[] indizes){
    	l.debug("AbstractDAO - separateArrays(int[] indizes): "+indizes);
    	columns = DFDUtilities.getStringList(DFDUtilities.separateArray(columns, indizes));
    	data = DFDUtilities.getStringList(DFDUtilities.separateArray(data, indizes));
    	typ = DFDUtilities.getIntList(DFDUtilities.getIntegerListOfObject(DFDUtilities.separateArray(DFDUtilities.getIntegerList(typ), indizes)));
    }
    
    @Override
    public T getElement() {
    	return element;
    }
    
    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public String[] getData() {
        return data;
    }

    @Override
    public int[] getTyp() {
        return typ;
    }
    @Override
	public String getTablename() {
		return tablename;
	}
    @Override
    public int getColumnCount() {
    	return columns.length;
    }
    public int getMaxLength() {
		return maxLength;
	}

}
