package at.htlpinkafeld.dfd.database.model;

import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.pojo.Benutzer_Person;
/**
 * Das Datenbanktabellenmodel f�r die Tabelle Benutzer_Person
 * @author Patrick
 *
 */
public class Benutzer_PersonModel extends AbstractModel<Benutzer_Person> {

	private static final long serialVersionUID = 4238281488660451147L;
	private static final int MAXLENGTH = 2;

	

	public Benutzer_PersonModel(Benutzer_Person element, int off, int set) {
		super(element, off, set, MAXLENGTH);
	}

	public Benutzer_PersonModel(Benutzer_Person element, int set) {
		super(element, set, MAXLENGTH);
	}

	public Benutzer_PersonModel(Benutzer_Person element) {
		super(element, MAXLENGTH);
	}

	public Benutzer_PersonModel(Benutzer_Person element, int[] columns) {
		super(element, columns, MAXLENGTH);
	}

	@Override
	public String[] initColumns() {
		String[] ret = new String[MAXLENGTH];
		ret[0] = "username";
		ret[1] = "id";
		return ret;
	}

	@Override
	public String[] initData() {
		String[] ret = new String[MAXLENGTH];
		ret[0] = element.getUser().getName();
		ret[1] = element.getPerson().getId()+"";
		return ret;
	}

	@Override
	public int[] initTyp() {
		int[] ret = new int[MAXLENGTH];
		for(int i=0; i < ret.length;i++)
			if(i == 0)
			ret[i] = DAO.STRING;
			else
				ret[i] = DAO.NUMBER;
		return ret;
	}
	
	@Override
	public String initTablename() {
		return "Benutzer_Person";
	}
	@Override
	protected String initPKS() {
		return null;
	}

	

}
