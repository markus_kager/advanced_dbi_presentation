package at.htlpinkafeld.dfd.database.model;

import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.pojo.User;
import at.htlpinkafeld.dfd.util.DFDUtilities;
/**
 * Das Datenbanktabellenmodel f�r die Tabelle Benutzer
 * @author Patrick
 *
 */
public class BenutzerModel extends AbstractModel<User> {

	private static final long serialVersionUID = 6656532459722532455L;
	private static int MAXLENGTH = 3;
	
	public BenutzerModel(User element, int off, int set) {
		super(element, off, set, MAXLENGTH);
	}

	public BenutzerModel(User element, int off) {
		super(element, off, MAXLENGTH);
	}

	public BenutzerModel(User element) {
		super(element, MAXLENGTH);
	}

	public BenutzerModel(User element, int[] columns) {
		super(element, columns, MAXLENGTH);
	}

	@Override
	public String[] initColumns() {
		l.debug("BenutzerModelDAO - initColumns()");
		String[] ret = new String[MAXLENGTH];
		ret[0] = "username";
		ret[1] = "password";
		ret[2] = "email";
		return ret;
	}

	@Override
	public String[] initData() {
		l.debug("BenutzerModelDAO - initData()");
		String[] ret = new String[MAXLENGTH];
		ret[0] = element.getName();
		ret[1] = DFDUtilities.decryptString(element.getPwd());
		ret[2] = element.getEmail();
		return ret;
	}

	@Override
	public int[] initTyp() {
		l.debug("BenutzerModelDAO - initTyp()");
		int[] ret = new int[MAXLENGTH];
		for(int i=0; i < ret.length;i++)
			ret[i] = DAO.STRING;
		return ret;
	}
	@Override
	public String initTablename() {
		l.debug("BenutzerModelDAO - initTablename()");
		return "Benutzer";
	}

	@Override
	protected String initPKS() {
		l.debug("BenutzerModelDAO - initPKS()");
		return null;
	}
}
