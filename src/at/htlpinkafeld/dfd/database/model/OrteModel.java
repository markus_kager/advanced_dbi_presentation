package at.htlpinkafeld.dfd.database.model;

import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.pojo.Pair;
/**
 * Das Datenbanktabellenmodel f�r die Tabelle Orte
 * @author Patrick
 *
 */
public class OrteModel extends AbstractModel<Pair> {

	private static final long serialVersionUID = -3505956121972752070L;
	private static final int MAXLENGTH = 2;
	
	public OrteModel(Pair element, int off, int set) {
		super(element, off, set, MAXLENGTH);
	}

	public OrteModel(Pair element, int off) {
		super(element, off, MAXLENGTH);
	}

	public OrteModel(Pair element) {
		super(element, MAXLENGTH);
	}

	public OrteModel(Pair element, int[] columns) {
		super(element, columns, MAXLENGTH);
	}

	@Override
	protected String[] initColumns() {
		String[] ret = new String[MAXLENGTH];
		ret[0] = "id";
		ret[1] = "ortsname";
		return ret;
	}

	@Override
	protected String[] initData() {
		String[] ret = new String[MAXLENGTH];
		ret[0] = element.getKey()+"";
		ret[1] = element.getValue();
		return ret;
	}

	@Override
	protected int[] initTyp() {
		int[] ret = new int[MAXLENGTH];
		ret[0] = DAO.STRING;
		ret[1] = DAO.STRING;
		return ret;
	}

	@Override
	protected String initTablename() {
		return "Orte";
	}

	@Override
	protected String initPKS() {
		return null;
	}
	
	

}
