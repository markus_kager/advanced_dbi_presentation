/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.database.model;

import at.htlpinkafeld.dfd.database.dao.DAO;
import at.htlpinkafeld.dfd.pojo.Schule_Klasse;

/**
 * Das Datenbanktabellenmodel f�r die Tabelle Schule_Klasse
 * @author Patrick
 *
 */
public class Schule_KlasseModel extends AbstractModel<Schule_Klasse>{
	

	private static final long serialVersionUID = 6537699941107927697L;
	private static int MAXLENGTH = 3; 
    
    public Schule_KlasseModel(Schule_Klasse element, int off, int set) {
		super(element, off, set, MAXLENGTH);
	}

	public Schule_KlasseModel(Schule_Klasse element, int set) {
		super(element, set, MAXLENGTH);
	}

	public Schule_KlasseModel(Schule_Klasse element) {
		super(element, MAXLENGTH);
	}
	public Schule_KlasseModel(Schule_Klasse element, int[] columns) {
		super(element, columns, MAXLENGTH);
	}

	@Override
    protected String[] initColumns() {
    	l.debug("Schule_KlasseInsertDAO - initColumns()");
    	String[] ret = new String[MAXLENGTH];
    	ret[0] = "id";
    	ret[1] = "schul_id";
    	ret[2] = "klasse";
    	return ret;
    }
    
    @Override
    public String[] initData() {
    	l.debug("Schule_KlasseInsertDAO - initData()");
        String[] ret = new String[MAXLENGTH];
        if(element.getKey() != 0)
        	ret[0] = element.getKey()+"";
        else
            ret[0] = pks+".nextval";
        ret[1] = element.getSchule().getKey()+"";
        ret[2] = element.getKlasse();
        
        return ret;
    }

    @Override
    public int[] initTyp() {
    	l.debug("Schule_KlasseInsertDAO - getTyp()");
        int [] ret = new int[MAXLENGTH];
        for(int i = 0; i < ret.length;i++)
            if(i == 2)
                ret[i] = DAO.STRING;
        else
                ret[i] = DAO.NUMBER;
        return ret;
    }
    @Override
    protected String initTablename() {
    	return "Schule_Klasse";
    }
    @Override
    protected String initPKS() {
    	return "SCHULE_KLASSE_PKS";
    }
}
