/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.database.model;

import java.io.Serializable;

/**
 * Das Model einer Tabelle in der Datenbank
 *
 * Kann f�r INSERT, DELETE, UPDATE und SELECT verwendet werden
 *
 * e.q. 
 * String[] columns = getColumns();
 * String[] data = getData();
 * int[] typ = getTyp();
 * String tablename = getTablename();
 *
 *  for(int i = 0; i 'kleiner' getColumnCount(); i++)
 *	System.out.println("Tabellenname: "+tablename+" Spalte["+i+"]: "+columns[i]+" Wert: "+data[i]+" Typ: "+typ[i]);
 *
 *
 * @author Patrick
 */
public interface Model<T> extends Serializable{
    /**
     * 
     * @return eine String[] mit den einzelnen Spalten der Tabelle
     */
    public String[] getColumns();
    /**
     * 
     * @return eine String[] mit den einzelnen Werten zu den jeweiligen Spalten 
     * 
     * Note: der Wert[0] geh�rt dann auch zur SPALTE[0]
     */
    public String[] getData();
    /**
     * 
     * @return eine int[] mit den Typen der Werte zu den jeweiligen Spalten 
     * 
     * Note: der Wert[0] geh�rt dann auch zur SPALTE[0]
     */
    public int[] getTyp();
    /**
     * 
     * @return der Tabellenname
     */
    public String getTablename();
    /**
     * 
     * @return Anzahl der Spalten
     */
    public int getColumnCount();
    /**
     * 
     * @return the Element of the Model
     */
    public T getElement();
    
    
    
   
    
}
