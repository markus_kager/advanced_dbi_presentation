package at.htlpinkafeld.dfd.database;

import java.io.Serializable;
/**
 * Wird verwendet um die MetaData zu generieren
 * @author Patrick
 *
 * @param <T> Type des Elements
 */
public class Value<T> implements Serializable {

	private static final long serialVersionUID = -5630930464909585181L;
	private T element;
	private Class<?> c;
	
	public Value() {
	}
	public Value(T element){
		this.element = element;
		this.c = element.getClass();
	}
	public T getElement() {
		return element;
	}
	public void setElement(T element) {
		this.element = element;
	}
	public Class<? extends Object> getC() {
		return c;
	}
	public void setC(Class<? extends Object> c) {
		this.c = c;
	}
	@Override
	public String toString() {
		return "Value [element=" + element + ", c=" + c + "]";
	}
	
}
