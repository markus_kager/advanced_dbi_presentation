/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.database;

import java.io.Serializable;
import java.util.List;


/**
 * Das Ergebnis, das gewisse Funktion von DAO zur�ckgibt
 * @author Patrick
 */
public class DAOResult implements Serializable{

	private static final long serialVersionUID = 8648629967957537528L;
	private boolean erg;
    private List<MetaData> metaData;
    public DAOResult() {
    }
	
	public boolean isErg() {
		return erg;
	}

	public void setErg(boolean erg) {
		this.erg = erg;
	}

	public List<MetaData> getMetaData() {
		return metaData;
	}


	public void setMetaData(List<MetaData> metaData) {
		this.metaData = metaData;
	}

	public DAOResult(boolean erg, List<MetaData> metaData) {
		super();
		this.erg = erg;
		this.metaData = metaData;
	}









    
 
    
}
