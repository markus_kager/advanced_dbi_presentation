/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.database.dao;

import org.apache.log4j.Logger;

/**
 *
 * @author Patrick
 */
public class DAOFactory {
	private final static Logger l = Logger.getLogger(DAOFactory.class);
    /**
     * 
     * @param name was f�r ein DAO Objekt erstellt wird
     * @return Das Interface DAO
     */
    public static DAO createDAO(String name){
		DAO ret = null;
		l.debug("DAOFactory - createDAO(String name): "+name);
        if(name.toLowerCase().equals("oracle"))
        {

            	ret = OracleDAO.newInstance();

        }
        
            return ret;
        
    }
    
}
