package at.htlpinkafeld.dfd.database.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;



import org.apache.log4j.Logger;

import at.htlpinkafeld.dfd.database.DAOResult;
import at.htlpinkafeld.dfd.database.MetaData;
import at.htlpinkafeld.dfd.database.Value;
import at.htlpinkafeld.dfd.pojo.Person;
import at.htlpinkafeld.dfd.pool.ConnectionManager;
import at.htlpinkafeld.dfd.util.DFDUtilities;
/**
 * hallo, das ist der bauer der testet
 * DAO Object das alle wichtigen SQL Funktionen implementiert die Werte kommen dann vom Model Element
 * @author Patrick
 *
 */
public abstract class AbstractDAO implements DAO {

	private static final long serialVersionUID = 642906470866498576L;
	protected static Logger l = Logger.getLogger(AbstractDAO.class);
    protected Connection cn;
    protected ResultSet results;
    protected String IP;
    protected Statement st;
    
    @Override
    public void command(String command) throws DAOException {
    	l.debug("OracleDAO - command(String command): "+command);
    	try(Statement s = cn.createStatement();
    		Connection cn = ConnectionManager.newInstance().getNewConnection()){           
            s.execute(command);
    	}catch(SQLException e){throw new DAOException(e);}
    }

    @Override
    public DAOResult select(String tablename) throws DAOException{
    	l.debug("OracleDAO - select(String tablename): "+tablename);
    	DAOResult ret = null;
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();
    			ResultSet results = st.executeQuery("SELECT * FROM "+tablename);){
    		ret = new DAOResult(true,convertResultSet(results));
    	}catch(SQLException e){throw new DAOException(e);}
        return ret;
    }

    @Override
    public void update(String tablename, String column,String set,String datawhere, String where,int type_set,int type)throws DAOException {
    	l.debug("OracleDAO - update(String tablename, String column,String set,String datawhere, String where,int type_set,int type): "+tablename+" , "+column+" , "+set+" , "+datawhere+" , "+where+" , "+type_set+" , "+type);
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();){
        if(type != DAO.NUMBER)
        where = "'"+where+"'";
        if(type_set != DAO.NUMBER)
        set = "'"+set+"'";

        st.executeUpdate("UPDATE "+tablename+" SET "+column+" = "+set+" WHERE "+datawhere+" = "+where);
    	}catch(SQLException e){throw new DAOException(e);}
    }
    
    @Override
    public void insert(String tablename, String[] columns, String[] data,int[] typ) throws DAOException {
    	l.debug("OracleDAO - insert(String tablename, String[] columns, String[] data,int[] typ): "+tablename+" , "+DFDUtilities.ArrayToString(columns)+" , "+DFDUtilities.ArrayToString(data)+" , "+DFDUtilities.ArrayToString(typ));
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();){
        StringBuilder b = new StringBuilder("INSERT INTO "+tablename);
        if(columns.length > 0)
        	b.append("(");
        for(int i = 0; i<columns.length;i++){
            String str = columns[i];
            b.append(str);
            if(i!=columns.length-1)
                b.append(",");
        }
        if(columns.length > 0)
        	b.append(")");
        b.append(" VALUES(");
        for(int i = 0; i<data.length;i++){
            String str = data[i];
            if(typ[i]!=DAO.NUMBER)
            b.append("'").append(str).append("'");
            else
                b.append(str);
            if(i!=data.length-1)
                b.append(",");
        }
        b.append(")");
        l.debug(b.toString());
        st.executeUpdate(b.toString());
    	}catch(SQLException e){throw new DAOException(e);}
    }
  
    @Override
    public void insert(String tablename, String[] data,int[] typ)throws DAOException {
    	l.debug("OracleDAO - insert(String tablename, String[] data,int[] typ: "+tablename+" , "+DFDUtilities.ArrayToString(data)+" , "+DFDUtilities.ArrayToString(typ));
        String[] columns = new String[0];
        insert(tablename, columns, data, typ);
    }

    @Override
    public void delete(String tablename,String[] columns, String[] where,int[] typ,String[] and_or) throws DAOException{
    	l.debug("OracleDAO - delete(String tablename,String[] columns, String[] where,int[] typ,String[] and_or): "+tablename+" , "+DFDUtilities.ArrayToString(columns)+" , "+where+" , "+typ+" , "+and_or);
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();){
    	StringBuilder b = new StringBuilder();
    	b.append("DELETE "+tablename+" WHERE ");
    	for(int i = 0; i < columns.length; i++){
	        if(typ[i] != DAO.NUMBER)
	        	where[i] = "'"+where[i]+"'";
	        
	        if(i != 0)
	        	b.append(and_or[i-1]+" ");
	        b.append(columns[i]+" = "+where[i]+" ");
    	}
        l.debug(b.toString());
        st.executeUpdate(b.toString());
    	}catch(SQLException e){throw new DAOException(e);}
    }
    
    @Override
    public void delete(String tablename,String data, String where,int typ) throws DAOException{
    	l.debug("OracleDAO - delete(String tablename,String data, String where,int typ): "+tablename+" , "+data+" , "+where+" , "+typ);
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();){
        if(typ != DAO.NUMBER){
        where = "'"+where+"'";
        }
        st.executeUpdate("DELETE "+tablename+" WHERE "+data+" = "+where);
    	}catch(SQLException e){throw new DAOException(e);}
    }

    @Override
    public DAOResult find(String tablename, String[] columns, String[] where,int[] typ, String[] and_or) throws DAOException {
    	l.debug("OracleDAO - find(String tablename, String[] columns, String[] where,int[] typ, String[] and_or): "+tablename+" , "+DFDUtilities.ArrayToString(columns)+" , "+DFDUtilities.ArrayToString(where)+" ,"+DFDUtilities.ArrayToString(typ)+" , "+DFDUtilities.ArrayToString(and_or));
        DAOResult ret = new DAOResult();
        StringBuilder b = new StringBuilder();
        b.append("SELECT * FROM ").append(tablename).append(" WHERE");
        for(int i = 0; i < where.length; i++)
        {
            if(typ[i] != DAO.NUMBER)
            	where[i] = "'"+where[i]+"'";
            if(i != 0)
                b.append(and_or[i-1]);
            
            b.append(" LOWER (").append(columns[i]).append(") = LOWER(");
            if(typ[i] == DAO.DATE)
                b.append("TO_DATE(");
            b.append(where[i]);
            where[i] = where[i].replaceAll("\'", "");
            if(typ[i] == DAO.DATE)
                b.append(",\'").append(Person.FORMAT.toPattern()).append("\')");        
            b.append(") ");
            
        }
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();
    			ResultSet results = st.executeQuery(b.toString());){
        if(results.next()){
            ret.setErg(true);
            ret.setMetaData(convertResultSet(st.executeQuery(b.toString())));
        }
        else
        {
        	ret.setErg(false);
        	ret.setMetaData(convertResultSet(st.executeQuery(b.toString())));
        }
    	}catch(SQLException e){throw new DAOException(e);}
    	
        return ret;
    }
    @Override
    public DAOResult identicalFind(String tablename, String[] columns, String[] where,int[] typ, String[] and_or) throws DAOException {
    	l.debug("OracleDAO - find(String tablename, String[] columns, String[] where,int[] typ, String[] and_or): "+tablename+" , "+DFDUtilities.ArrayToString(columns)+" , "+DFDUtilities.ArrayToString(where)+" ,"+DFDUtilities.ArrayToString(typ)+" , "+DFDUtilities.ArrayToString(and_or));
        DAOResult ret = new DAOResult();
        StringBuilder b = new StringBuilder();
        b.append("SELECT * FROM ").append(tablename).append(" WHERE");
        for(int i = 0; i < where.length; i++)
        {
            if(typ[i] != DAO.NUMBER)
        where[i] = "'"+where[i]+"'";
            if(i != 0)
                b.append(and_or[i-1]);
            
            b.append(" ").append(columns[i]).append(" = ");
            if(typ[i] == DAO.DATE)
                b.append("TO_DATE(");
            b.append(where[i]);
            where[i] = where[i].replaceAll("\'", "");
            if(typ[i] == DAO.DATE)
                b.append(",\'").append(Person.FORMAT.toPattern()).append("\')");        
            b.append(" ");
            
        }
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();
    			ResultSet results = st.executeQuery(b.toString());){
        if(results.next()){
            ret.setErg(true);
            ret.setMetaData(convertResultSet(st.executeQuery(b.toString())));
        }
        else
        {
        	ret.setErg(false);
        	ret.setMetaData(convertResultSet(st.executeQuery(b.toString())));
        }
    	}catch(SQLException e){throw new DAOException(e);}
    	
        return ret;
    }
    @Override
    public DAOResult find(String tablename, String columns, String where,int typ) throws DAOException {
    	l.debug("OracleDAO - find(String tablename, String columns, String where,int typ): "+tablename+" , "+columns+" , "+where+" ,"+typ);
        String[] mydata = new String[1];
        String[] mywhere = new String[1];
        int[] myTyp = new int[1];
        mydata[0] = columns;
        mywhere[0] = where;
        myTyp[0] = typ;
        return find(tablename, mydata, mywhere, myTyp, mydata);
    }
    
    
    @Override
    public DAOResult search(String tablename, String columns, String where,int suche, int sort) throws DAOException {
    	l.debug("OracleDAO - search(String tablename, String columns, String where): "+tablename+" , "+columns+" , "+where);
    	DAOResult ret = find(tablename, columns, where,DAO.STRING);
        if(!ret.isErg())
        {
        if(suche == PROZENT_LINKS_RECHTS)
        where = "'%"+where+"%'";
        if(suche == PROZENT_RECHTS)
        	where = "'"+where+"%'";
        if(suche == PROZENT_LINKS)
        	where = "'%"+where+"'";
        String str = "SELECT * FROM "+tablename+" WHERE LOWER("+columns+") like LOWER("+where+")";
        if(where == "") {
        	str = "SELECT * FROM "+tablename;             //alle Personen Suchen und Ausgeben
        }
       
       /* if(sort==1) {
        	str = str + " ORDER BY vorname asc ";
        }
        if(sort==2) {
        	str = str + "ORDER BY vorname desc ";
        }
        if(sort==0) {}*/
        
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();
    			ResultSet results = st.executeQuery(str);){ 
        if(results.next())
        	ret = new DAOResult(true,convertResultSet(st.executeQuery(str)));   
        else      
        	ret = new DAOResult(false,convertResultSet(st.executeQuery(str)));
        
    	}catch(SQLException e){throw new DAOException(e);}
        }
       
        return ret;  
       
    }
    
    @Override
    public DAOResult topNAnalyse(String tablename, int border,String operator, String[] columns,String orderColumn, String order) throws DAOException{
    	l.debug("OracleDAO - topNAnalyse(String tablename, int border, String operator, String[] columns, String orderColumn,  String order): "+tablename+" , "+border+" , "+operator+" , "+DFDUtilities.ArrayToString(columns)+" , "+orderColumn+" , "+order);
    	DAOResult ret = null;
        StringBuilder b = new StringBuilder();
        b.append("SELECT ");
        for(int i = 0; i < columns.length; i++)
        {
        	if(i != 0)
        		b.append(", ");
        	b.append(columns[i]);       	
        }
        b.append(" FROM ( SELECT ");
        for(int i = 0; i < columns.length; i++)
        {
        	if(i != 0)
        		b.append(", ");
        	b.append(columns[i]);       	
        }
        b.append(" FROM "+tablename +" ORDER BY "+orderColumn +" "+order+")");
        b.append("WHERE ROWNUM "+operator+" "+border);
    	try(Connection cn = ConnectionManager.newInstance().getNewConnection();
    			Statement st = cn.createStatement();
    			ResultSet results = st.executeQuery(b.toString());){
        if(results.next()){
        	ret = new DAOResult();
            ret.setErg(true);
            ret.setMetaData(convertResultSet(st.executeQuery(b.toString())));
        }
    	}catch(SQLException e){throw new DAOException(e);}
        return ret;
    }
    /**
     * 	Wandelt ResultSet in Liste aus MetaData um
     * @param r das umzuwnadelnde ResultSet
     * @return Liste aus MetaData
     * @throws SQLException
     */
    private List<MetaData> convertResultSet(ResultSet r) throws SQLException{
    	l.debug("AbstractDAO - List<MetaData> convertResultSet(ResultSet r): "+r);
    	List<MetaData> ret = new ArrayList<>();
    	MetaData md = null;
    	while(r.next()){
    		ResultSetMetaData data = r.getMetaData();
    		md = new MetaData();
    		for(int i = 1; i <= data.getColumnCount(); i++){
    		if(data.getColumnType(i) == Types.NUMERIC || data.getColumnType(i) == Types.INTEGER || data.getColumnType(i) == Types.DECIMAL)
    			md.add(i, new Value<Integer>(r.getInt(i)), DAO.NUMBER);
    		else if(data.getColumnType(i) == Types.VARCHAR)
    			md.add(i, new Value<String>(r.getString(i)), DAO.STRING);
    		else if(data.getColumnType(i) == Types.TIMESTAMP || data.getColumnType(i) == Types.TIME || data.getColumnType(i) == Types.DATE)
    			md.add(i, new Value<Date>(r.getDate(i)), DAO.DATE);
    		else throw new SQLException("Wrong typ: "+data.getColumnType(i));
    		}
    		ret.add(md);
    	}
    	return ret;
    }

}
