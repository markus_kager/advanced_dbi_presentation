/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.database.dao;

import org.apache.log4j.Logger;

/**
 * Ein DAO Object das eine Verbindung mit einer Oracle Datenabnk ermöglicht.
 * 
 * @author Patrick
 */
public class OracleDAO extends AbstractDAO{
    
	private static final long serialVersionUID = -2303663099793916452L;
    private static OracleDAO INSTANCE;
     
    public OracleDAO() {
    	l = Logger.getLogger(OracleDAO.class);
    	
	}

	/**
     * Erzeugt eine DAO.AND Liste mit der Anzahl 'size'.
     * 
     * @param size	Die Anzahl der Elemente DAO.AND
     * @return die Liste aus DAO.AND Elementen
     */
    public static String[] getAndList(int size){
    	l.debug("OracleDAO - getAndList(int size): "+size);
    	String[] ret = null;
    	if(size > 0){
    		ret = new String[size];
    	for(int i = 0; i < size; i++)
    		ret[i] = AND;
    	}
    	return ret;
    }
    /**
     * Es wird eine Instance erzeugt die eine Verbindung zur Oracle Datenbank hat
     * !die Verbindung ist ein Singelton!
     * 
     * @return die OracleDAO Instance
     */
    public static OracleDAO newInstance(){
    	if(INSTANCE == null)
    		INSTANCE = new OracleDAO();
    	return INSTANCE;
    }
    
    
    
}