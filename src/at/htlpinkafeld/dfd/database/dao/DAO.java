/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.database.dao;

import java.io.Serializable;

import at.htlpinkafeld.dfd.database.DAOResult;



/**
 *
 * @author Patrick
 */
public interface DAO extends Serializable{
    public static String AND = "AND",
                      OR = "OR",
                      ASC = "ASC",
                      DESC="DESC",
                      KLEINER = "<",
                      KLEINER_GLEICH="<=",
                      GR�SSER=">",
                      GR�SSER_GLEICH=">=";
    public static int STRING = 0,
                      DATE = 1,
                      NUMBER = 2,
                      PROZENT_LINKS_RECHTS = 100,
                      PROZENT_RECHTS = 101,
    				  PROZENT_LINKS = 102;
    
    
    /**
     * Es wird ein SELECT * FROM {tablename} aufgerufen
     * 
     * @param tablename	Der Name der zu selektierenden Tabelle
     * 
     * @return	Das Result des select Befehls
     * 
     * @throws DAOException Exception
     * 
     */
    public DAOResult select(String tablename)throws DAOException;
    /**
     * Es wird ein UPDATE {tablename} SET {column} = {set} WHERE {columnwhere} = {where} aufgerufen
     * 
     * @param tablename		Der Name der zu updatenden Tabelle
     * @param column 		Die zu updatende Spalte
     * @param set 			Der neue Wert der zu updatenden Spalte
     * @param columnwhere	Spalte in der WHERE Bedingung
     * @param where			Wert der in der WHERE-Bedingung erf�llt werden muss
     * @param typ_set		Der Typ der Spalte vom Set Befehl
     * @param typ			Der Typ der Spalte in der WHERE-Bedingung
     * 
     * @throws DAOException Exception
     */
    public void update(String tablename,String column,String set,String columnwhere,String where,int typ_set,int typ)throws DAOException;
    /**
     * 
     * Es wird ein INSERT INTO {tablename} ({columns[]}) VALUES ({data[]})  aufgerufen
     * 
     * @param tablename		Der Name Tabelle in die eingef�gt wird
     * @param columns 		Die Spalten in de eingef�gt werden
     * @param data 			Die Werte die in die Spalten eingef�gt werden
     * @param typ			Die Typen der Spalten
     * 
     * @throws DAOException Exception
     */
    public void insert(String tablename,String[] columns,String[] data, int[] typ)throws DAOException;
    /**
     * Es wird ein INSERT INTO {tablename} VALUES ({data[]})  aufgerufen
     * 
     * @param tablename		Der Name Tabelle in die eingef�gt wird
     * @param data			Die Werte die in die Spalten eingef�gt werden
     * @param typ			Die Typen der Spalten
     * 
     * @throws DAOException Exception
     */
    public void insert(String tablename,String[] data, int[] typ)throws DAOException;
    /**
     * Es wird ein DELETE {tablename} WHERE {column} = {where}  aufgerufen
     * 
     * @param tablename		Der Name Tabelle aus der ein Satz gel�scht wird
     * @param column 		Die Spalte in de WHERE-Bedingung
     * @param where 		Der Wert in der WHERE-Bedingung
     * @param typ			Der Typ der Spalte in der WHERE-Bedingung
     * 
     * @throws DAOException Exception
     */
    public void delete(String tablename,String column, String where,int typ)throws DAOException;
    /**
     * Es wird ein DELETE {tablename} WHERE {column} = {where} {and_or} {column} = {where} ... aufgerufen 
     * 
     * @param tablename		Der Name Tabelle aus der ein Satz gel�scht wird
     * @param columns 	Die Spalten in de WHERE-Bedingung
     * @param where		Die Werte in der WHERE-Bedingung
     * @param typ			Der Typen der Spalte in der WHERE-Bedingung
     * @param and_or		Ob die WHERE Bedingungen mit AND oder OR zusammengeh�ngt werden
     * 
     * @throws DAOException Exception
     */
    public void delete(String tablename,String[] columns, String[] where,int[] typ,String[] and_or)throws DAOException;
    /**
     * Es wird ein SELECT * FROM {tablename} WHERE {column} like '%{where}%' aufgerufen 
     * 
     * @param tablename		Der Name Tabelle in der gesucht wird
     * @param column		Die Spalte in de WHERE-Bedingung
     * @param where 		Der Wert in der WHERE-Bedingung
     * @param suche			Wie nach dem Suchbegriff gesucht wird(in DAO sind die Integerwerte vordefiniert)
     * 
     * @return	DAOResult die Werte die gefunden worden sind aus dem SELECT Statement
     * @throws DAOException Exception
     */
    public DAOResult search(String tablename,String column, String where, int suche, int sort) throws DAOException;
    /**
     * Es wird ein SELECT * FROM {tablename} WHERE {column} = {where} aufgerufen
     * 
     * @param tablename		Der Name Tabelle in der gesucht wird
     * @param column		Die Spalte in de WHERE-Bedingung
     * @param where 		Der Wert in der WHERE-Bedingung
     * @param typ			Der Typ der Spalte in der WHERE-Bedingung
     * 
     * @return	DAOResult die Werte die gefunden worden sind aus dem SELECT Statement
     * @throws DAOException Exception
     */
    public DAOResult find(String tablename,String column, String where,int typ) throws DAOException;
    /**
     * Es wird ein SELECT * FROM {tablename} WHERE LOWER{column} = LOWER{where} {and_or}  LOWER{column} = LOWER{where} ... aufgerufen 
     * 
     * @param tablename	Der Name Tabelle in der gesucht wird
     * @param columns	Die Spalten in de WHERE-Bedingung
     * @param where		Die Werte in der WHERE-Bedingung
     * @param typ		Die Typen der Spalte in der WHERE-Bedingung
     * @param and_or	Ob die Abfragen in der WHERE-Bedingung mit AND oder OR zusammengeh�ngt werden
     * @return			Das DAOResult mit dem Ergebnis
     * @throws DAOException Exception
     * 
     * @return	DAOResult die Werte die gefunden worden sind aus dem SELECT Statement
     * 
     */
    public DAOResult find(String tablename,String[] columns,String[] where,int[] typ,String[] and_or)throws DAOException;
    /**
     * Es wird ein SELECT * FROM {tablename} WHERE {column} = {where} {and_or}  {column} = {where} ... aufgerufen
     * 
     * @param tablename	Der Name Tabelle in der gesucht wird
     * @param columns	Die Spalten in de WHERE-Bedingung
     * @param where		Die Werte in der WHERE-Bedingung
     * @param typ		Die Typen der Spalte in der WHERE-Bedingung
     * @param and_or	Ob die Abfragen in der WHERE-Bedingung mit AND oder OR zusammengeh�ngt werden
     * @return			Das DAOResult mit dem Ergebnis
     * @throws DAOException Exception
     * 
     */
    public DAOResult identicalFind(String tablename,String[] columns,String[] where,int[] typ,String[] and_or)throws DAOException;
    /**
     * Es wird ein {command} aufgerufen
     * 
     * @param command	der SQL Befehl als String der dann ausgef�hrt wird
     * @throws DAOException Exception
     */
    public void command(String command) throws DAOException;
    /**
     * Es wird ein SELECT {columns} FROM (SELECT {columns} FROM {tablename} ORDER BY {orderColumn} {order}) WHERE ROWNUM {operator} {border} aufgerufen
     * 
     * @param tablename		Der Name Tabelle in der gesucht wird
     * @param border		Die Grenze bei der Top-N-Analyse
     * @param operator		Der Operator bie der Abfrage ROWNUM {operator} {border}; operator: (KLEINER,KLEINER_GLEICH,GR��ER, GR��er_GLEICH)
     * @param columns 	Die Spalten in de WHERE-Bedingung
     * @param orderColumn	Die zu sortierende Spalte
     * @param order			Ob aufsteigend oder absteigend sortiert wird (ASC,DESC)
     * 
     * @return	DAOResult die Werte die gefunden worden sind aus der Top-N-Analyse
     * @throws DAOException Exception
     */
    public DAOResult topNAnalyse(String tablename, int border,String operator, String[] columns, String orderColumn, String order) throws DAOException;
    
    
}
