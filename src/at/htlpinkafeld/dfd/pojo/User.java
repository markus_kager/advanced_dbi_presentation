package at.htlpinkafeld.dfd.pojo;

import java.io.Serializable;
/**
 * Ist das Model der Tabelle Benutzer
 * @author Patrick
 *
 */
public class User implements Serializable,Comparable<User> {

private static final long serialVersionUID = -8207778712282035038L;
private String name,
			   pwd,
			   email;
private boolean login;

public User() {
}

public User(String name,String pwd,String email){
	this.name = name;
	this.pwd = pwd;
	this.email = email;
}

public String getEmail() {
	return email;
}
public String getName() {
	return name;
}
public String getPwd() {
	return pwd;
}
public void setEmail(String email) {
	this.email = email;
}
public void setName(String name) {
	this.name = name;
}
public void setPwd(String pwd) {
	this.pwd = pwd;
}
public boolean isLogin() {
	return login;
}
public void setLogin(boolean login) {
	this.login = login;
}
@Override
	public String toString() {
		return name+","+pwd+","+email;
	}

@Override
	public int compareTo(User o) {
	int ret = 0;
	if(name != null && o.name != null)
		ret = name.compareTo(o.name);
	else
		return -1;
	if(ret == 0)
		if(pwd != null && o.pwd != null)
			ret = pwd.compareTo(o.pwd);
		else
			return -1;
	if(ret == 0)
		if(email != null && o.email != null)
			ret = email.compareTo(o.email);
		else
			return -1;
	
	return ret;
	}
@Override
	public boolean equals(Object obj) {
	boolean ret = false;
	if(obj != null)
		if(obj instanceof User)
			if(compareTo((User)obj) == 0)
				ret = true;
	return ret;
	}
}

