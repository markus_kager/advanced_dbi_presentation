package at.htlpinkafeld.dfd.pojo;

import java.io.Serializable;
/**
 * Ist das Model der Tabelle Benutzer_Person
 * @author Patrick
 *
 */
public class Benutzer_Person implements Serializable,Comparable<Benutzer_Person>{
	
	private static final long serialVersionUID = -1027619392277787799L;
	private User user;
	private Person person;
	
	public Benutzer_Person() {
	}

	
	public Benutzer_Person(User user, Person person) {
		this.user = user;
		this.person = person;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Person getPerson() {
		return person;
	}


	public void setPerson(Person person) {
		this.person = person;
	}


	@Override
	public int compareTo(Benutzer_Person o) {
		return o.user.compareTo(this.user)-o.person.compareTo(this.person);
	}
	@Override
	public boolean equals(Object obj) {
		boolean ret = false;
		if(obj != null)
			if(obj instanceof Benutzer_Person)
				if(this == obj)
					ret = true;		
				else{
					if(this.compareTo((Benutzer_Person)obj) == 0)
						ret = true;
				}
		return ret;
	}

	@Override
	public String toString() {
		return "Benutzer_Person [user=" + user + ", person=" + person + "]";
	}
}
