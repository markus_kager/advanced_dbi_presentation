/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.pojo;

import java.io.Serializable;

/**
 * Ist das Model der Tabelle Schule_Klasse
 * @author Patrick
 *
 */
public class Schule_Klasse implements Serializable,Comparable<Schule_Klasse>{
    
	private static final long serialVersionUID = -5267923828121873407L;
	private int key;
    private Pair schule;
    private String klasse;
    
    
    public Schule_Klasse() {
        schule = new Pair();
    }
    public Schule_Klasse(Schule_Klasse schule_klasse){
    	key = schule_klasse.getKey();
    	schule = new Pair(schule_klasse.getSchule());
    	klasse = schule_klasse.getKlasse();
    }

    public Schule_Klasse(int key, Pair schule, String klasse) {
        this.key = key;
        this.schule = schule;
        this.klasse = klasse;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public Pair getSchule() {
        return schule;
    }

    public void setSchule(Pair schule) {
        this.schule = schule;
    }

    public String getKlasse() {
        return klasse;
    }

    

    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }

    @Override
    public int compareTo(Schule_Klasse o) {
    	int ret = -1;
    	if(schule != null && o.schule != null)
    		ret = o.schule.compareTo(schule);
    	else
    		return -1;
        if(ret == 0)
        	if(o.klasse != null && klasse != null)
        		ret = o.klasse.compareTo(klasse);
        	else
        		return -1;
        return ret;
    }
    
    @Override
    public boolean equals(Object obj) {
    	boolean ret = false;
    	if(obj != null)
    	if(obj instanceof Schule_Klasse)
    	{
    		if(compareTo((Schule_Klasse)obj) == 0)
    			ret = true;
    	}
    	return ret;
    }
    
    
    
}
