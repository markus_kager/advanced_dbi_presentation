/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.pojo;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import at.htlpinkafeld.dfd.util.DFDUtilities;

/**
 * Ist das Model der Tabelle Person
 * @author Markus
 *
 */
public class Person implements Serializable,Comparable<Person>{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5073610292048764173L;
	public static SimpleDateFormat FORMAT = new SimpleDateFormat("dd.MM.yyyy");;
    private int id;
    private String vname,
                   nname;
    private Pair ort;
    private Schule_Klasse schule_klasse;
    /*
        false = Mann
        true = Frau
    */
    private boolean geschlecht;
    private Date gebDate;
    
    public Person() {
        this(null,null,new Pair(),new Schule_Klasse(),false,null);
    }

    public Person(String vname, String nname,int ort_key,String ort_name,int schule_klasse_key,int schule_key,String schule_name,String klasse,  boolean geschlecht, Date gebDate) {
        this(vname,nname,new Pair(ort_key, ort_name),new Schule_Klasse(schule_klasse_key,new Pair(schule_key, schule_name),klasse),geschlecht,gebDate);
    }
    public Person(int id,String vname, String nname,int ort_key,String ort_name,int schule_klasse_key,int schule_key,String schule_name,String klasse,  boolean geschlecht, Date gebDate) {
        this(id,vname,nname,new Pair(ort_key, ort_name),new Schule_Klasse(schule_klasse_key,new Pair(schule_key, schule_name),klasse),geschlecht,gebDate);
    }
    
    public Person(Person p){
    	this(p.getId(),p.getVname(),p.getNname(),p.getOrt(),p.getSchule_klasse(),p.isGeschlecht(),p.getGebDate());
    }

    public Person(String vname, String nname, Pair ort, Schule_Klasse schule_klasse, boolean geschlecht, Date gebDate) {
        this(0,vname,nname,ort,schule_klasse,geschlecht,gebDate);
    }
    
    public Person(int id,String vname, String nname, Pair ort, Schule_Klasse schule_klasse, boolean geschlecht, Date gebDate) {
        this.id = id;
        this.vname = vname;
        this.nname = nname;
        this.ort = new Pair(ort);
        this.schule_klasse = new Schule_Klasse(schule_klasse);
        this.geschlecht = geschlecht;
        this.gebDate = gebDate;
    }
    
    
    public static void setFORMAT(SimpleDateFormat FORMAT) {
        Person.FORMAT = FORMAT;
    }

    public static SimpleDateFormat getFORMAT() {
        return FORMAT;
    }

    @Override
    public String toString() {
        return vname+","+nname+","+ort.getValue()+","+DateAsString(gebDate)+","+DFDUtilities.booleanAsString(geschlecht)+","+schule_klasse.getSchule().getValue()+","+schule_klasse.getKlasse();
    }
    
   
    
    public String DateAsString(Date d){
    	if(d != null)
    		return FORMAT.format(gebDate);
    	else
    		return null;
    }

    public Date getGebDate() {
        return gebDate;
    }

    public void setGebDate(Date gebDate) {
        this.gebDate = gebDate;
    }

    public String getNname() {
        return nname;
    }

    public String getVname() {
        return vname;
    }

    public boolean isGeschlecht() {
        return geschlecht;
    }

    public void setGeschlecht(boolean geschlecht) {
        this.geschlecht = geschlecht;
    }

    public void setNname(String nname) {
        this.nname = nname;
    }

    public void setVname(String vname) {
        this.vname = vname;
    }

    public Pair getOrt() {
        return ort;
    }


    public void setOrt(Pair ort) {
        this.ort = ort;
    }

    public Schule_Klasse getSchule_klasse() {
        return schule_klasse;
    }

    public void setSchule_klasse(Schule_Klasse schule_klasse) {
        this.schule_klasse = schule_klasse;
        
    }
    public int getId() {
		return id;
	}
    public void setId(int id) {
		this.id = id;
	}

    @Override
    public int compareTo(Person o) {
        int ret = 0;
        if(o == null)
        	return -1;
        else{
        	ret = id - o.id;
        if(ret == 0)
        	if(o.vname != null && vname != null)
        		ret = vname.compareTo(o.vname);
        	else
        		return -1;
        if(ret == 0)
        	if(o.nname != null && nname != null)
        		ret = nname.compareTo(o.nname);
        	else
        		return -1;
        if(ret == 0)
        	if(o.ort != null && ort != null)
        		ret = ort.compareTo(o.ort);
        	else
        		return -1;
        if(ret == 0)
        	if(o.gebDate != null && gebDate != null)
        		ret = gebDate.compareTo(o.gebDate);
        	else
        		return -1;
        if(ret == 0)
        	if(o.schule_klasse != null && schule_klasse != null)
        		ret = schule_klasse.compareTo(o.schule_klasse);
        	else
        		return -1;
        if(geschlecht != o.geschlecht)
            ret = -1;
        }
        return ret;
    }
    @Override
    public boolean equals(Object obj) {
    	boolean ret = false;
    	if(obj != null)
    		if(obj instanceof Person)
    			if(compareTo((Person)obj) == 0)
    				ret = true;
    	return ret;
    }
    
    public String getFormatedGebDate(){
    	String ret = null;
    	try{
    		ret = FORMAT.format(gebDate);
    	}catch(NullPointerException e){
    		ret = null;
    	}
    	return ret;
    }
    
   
    public void info(){
    	StringBuilder sb = new StringBuilder();
    	sb.append("select * from user").append(" where ").append("vorname = ").append(this.vname).append(" and ").append("nachname = ").append(this.nname).append(";");
    	String s = sb.toString();
    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,s," "));
    }
   
    
    
    
}
