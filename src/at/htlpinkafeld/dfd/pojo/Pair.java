/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.htlpinkafeld.dfd.pojo;

import java.io.Serializable;

/**
 * Ist das Model der Tabelle Orte und Schule
 * @author Patrick
 *
 */
public class Pair implements Serializable,Comparable<Pair>{

	private static final long serialVersionUID = -1681061979107134166L;
	private int key;
    private String value;

    public Pair() {
    }
    
    public Pair(Pair p){
    	this(p.getKey(),p.getValue());
    }

    public Pair(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Pair{" + "key=" + key + ", value=" + value + '}';
    }

    @Override
    public int compareTo(Pair o) {
        int ret = key - o.key;
        if(ret == 0)
        	if(o.value != null && value != null)
        		ret = value.compareTo(o.value);
        	else
        		return -1;
        return ret;
    }
    @Override
    public boolean equals(Object o) {
    	boolean ret = false;
    	if(o != null)
    	if(o instanceof Pair){
    		if(compareTo((Pair)o) == 0)
    			ret = true;
    	}
    	return ret;
    }
    
    
    
    
}
