package at.htlpinkafeld.dfd.pojo;

import java.io.Serializable;
import java.util.Date;

import at.htlpinkafeld.dfd.util.DFDUtilities;
/**
 * Ist das Model die View Person_View
 * @author Patrick
 *
 */
public class Person_View implements Serializable, Comparable<Person_View> {


	private static final long serialVersionUID = 3811704465852834418L;
	private Person p;
	private Schule_Klasse schule_klasse;
	private Pair ort;
	private Pair schule;
	
	public Person_View() {
		ort = new Pair();
		schule = new Pair();
		schule_klasse = new Schule_Klasse();
		p = new Person();
	}

	public Person_View(Person p, Schule_Klasse schule_klasse, Pair ort,
			Pair schule) {
		super();
		this.p = p;
		this.schule_klasse = schule_klasse;
		this.ort = ort;
		this.schule = schule;
	}

	//Wichtig f�r Reflection
	public Person_View(int id, String vname,String nname,int ortKey,String ortsname,Date gebDate,int gesch,int schule_klasseKey,int schuleKey,String schulname,String klasse){
		this.p = new Person(id, vname, nname, ortKey, ortsname, schule_klasseKey, schuleKey, schulname, klasse, DFDUtilities.intToBoolean(gesch), gebDate);
		this.ort = new Pair(ortKey, ortsname);
		this.schule_klasse = new Schule_Klasse(schule_klasseKey, new Pair(schuleKey, schulname), klasse);
		this.schule = new Pair(schuleKey, schulname);
	}




	public Pair getSchule() {
		return schule;
	}


	public void setSchule(Pair schule) {
		this.schule = schule;
	}

	public Person getP() {
		return p;
	}


	public void setP(Person p) {
		this.p = p;
	}


	public Schule_Klasse getSchule_klasse() {
		return schule_klasse;
	}


	public void setSchule_klasse(Schule_Klasse schule_klasse) {
		this.schule_klasse = schule_klasse;
	}


	public Pair getOrt() {
		return ort;
	}


	public void setOrt(Pair ort) {
		this.ort = ort;
	}


	@Override
	public int compareTo(Person_View o) {
		int ret = 0;
		ret = o.p.compareTo(this.p);
		if(ret == 0)
			ret = o.schule_klasse.compareTo(this.schule_klasse);
		if(ret == 0)
			ret = o.ort.compareTo(this.ort);
		if(ret == 0)
			ret = o.schule.compareTo(this.schule);
		return ret;
	}
	@Override
	public boolean equals(Object obj) {
		boolean ret = false;
		if(obj != null){
			if(obj instanceof Person_View)
				if(this.compareTo((Person_View)obj) == 0)
					ret = true;
		}
		return ret;
	}

	@Override
	public String toString() {
		return "Person_View [p=" + p + ", schule_klasse=" + schule_klasse
				+ ", ort=" + ort + ", schule=" + schule + "]";
	}

}
