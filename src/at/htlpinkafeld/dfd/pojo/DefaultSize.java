package at.htlpinkafeld.dfd.pojo;

import java.io.Serializable;

public class DefaultSize implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public double td1,
				  td2;
	public DefaultSize() {
		
	}
	public int use;
	public double getTd1() {
		return td1;
	}
	public void setTd1(double td1) {
		this.td1 = td1;
	}
	public double getTd2() {
		return td2;
	}
	public void setTd2(double td2) {
		this.td2 = td2;
	}
	public int getUse() {
		return use;
	}
	public void setUse(int use) {
		this.use = use;
	}
	
	
}
