var w;
var h;
var imgh;
var position;
var pos;
var posl;
var scaleL = new Object();

$(document).ready(function(){
            w = $('#form\\:grid').width();
            h = $('#form\\:grid').height();
            imgh = h;
            position = $('#form\\:grid').position();
            //$('#form\\:grid').click(showDBProcess);
        });

var aktivMove;
var aktivScale;
var scale = 1.0;
var defaultSize = new Object();

function showDBProcess(){
	removeColumn();
	removeLast();
	setTimeout(function(){
		aktivScale = setInterval(scaleImg, 10);
		makeInvisible();
		addLetter("zu.gif", "closedLetter", "img3", 0);
		addLetter("offen.gif", "letter", "img1",1);
		addLetter("brief_animation_bottom.gif", "bottom_letter", "img2",3);
		addBox("mailbox_offen.gif", "mail_box", "img4",3);
		addBox("mailbox_vorne.gif", "mail_box_vorne", "img5",6);
		addBox("mailbox_animation.gif", "mail_box_animation", "img6",0);
		setTimeout(move, 800);
	}, 2000);	
}

function removeColumn(){
	if($('#form\\:LastInsertedPeople_data').children("tr").length <= 1){
		defaultSize.use = 1;
		defaultSize.td1 = $('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(0).width();
		defaultSize.td2 = $('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(1).width();
	}
	else
		{
		console.log($('#form\\:LastInsertedPeople_data').children("tr").length);
		defaultSize.use = 0;
		defaultSize.td1 = 0.0;
		defaultSize.td2 =0.0;
		}
	defaultSize.size = $('#form\\:LastInsertedPeople_data').children("tr").length;
	
		$('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("background","yellow","!important");
	    //$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("opacity","0.1","!important");
	    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("position","absolute");
	    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("opacity","0.0");
	/*$('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("background","yellow","!important");
    //$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("opacity","0.1","!important");
    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("position","absolute");
    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("opacity","0.0");*/
   /* $('#form\\:LastInsertedPeople_data').children("tr").eq(0).animate({
    	opacity: 0.0
    },1);*/
}

function removeLast(){
	/*if($('#form\\:LastInsertedPeople_data').children("tr").length <= 1){
		defaultSize.use = 1;
		defaultSize.td1 = $('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(0).width();
		defaultSize.td2 = $('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(1).width();
	}
	else
		{
		console.log($('#form\\:LastInsertedPeople_data').children("tr").length);
		defaultSize.use = 0;
		defaultSize.td1 = 0.0;
		defaultSize.td2 =0.0;
		}*/
	
	console.log(defaultSize.td1 + " td1");
	
	if($('#form\\:LastInsertedPeople_data').children("tr").length > 4){
	//$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("background","yellow","!important");
	$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("position","absolute");
	//$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("right","500px");
	$('#form\\:LastInsertedPeople_data').children("tr").eq(4).animate({
		opacity: 0.0,
		right: "-50px",
		width: "0px"
	},1000,function(){
		$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("opacity","1.0","!important");
		$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("position","relative");
		//$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("background","grey","!important");
	});
	//defaultSize.size = $('#form\\:LastInsertedPeople_data').children("tr").length;
	console.log("Size: "+$('#form\\:LastInsertedPeople_data').children("tr").length);
	console.log("DefaultSize: "+defaultSize);
	}

	//$('#form\\:LastInsertedPeople_data').children("tr").eq(4).css("position","relative");
	
}

function scaleImg(){
	scale = scale-0.01;
	if(scale >= 0.4)
		{
		$('#form\\:grid').css("transform", "scale("+scale+")");
		}
	else
		clearInterval(aktivScale);
}

function addBox(src,id,div,zIndex){
	$('#'+div).append('<img id="'+id+'" src="/DFD/resources/img/'+src+'" />');
	$('#'+id).css("transform", "scale("+w/200+","+h/200+")");
	$('#'+div).css("clear","both");
	$('#'+div).css("position", "absolute");
	$('#'+div).css("right",(w-200)+"px");
	$('#'+div).css("top",(100)+"px");
	$('#'+id).css("z-index", zIndex);
}

function addLetter(src,id,div,zIndex){
	$('#'+div).append('<img id="'+id+'" src="/DFD/resources/img/'+src+'" />');
	//console.log($('#letter').height());
	$('#'+id).css("transform", "scale("+w/160+","+h/160+")");
	$('#'+div).css("clear","both");
	$('#'+div).css("position", "absolute");
	$('#'+div).css("left",(w/2-10)+"px");
	$('#'+div).css("top",(h+100)+"px");
	$('#'+id).css("z-index", zIndex);
	imgh = h+80+$('#letter').height()*h/160;
	scaleL.w = w/160;
	scaleL.h = h/160;
}

function makeInvisible(){
	
	$('#form\\:back').css("visibility","hidden","!important");
	$('#form\\:send').css("visibility","hidden","!important");
}

function move(){
	//console.log($("#letter").height()*(h/160));
	//console.log($("#letter").position().top);
	//console.log($("#form\\:grid").height()/(h/160));
	$("#form\\:grid").animate({
		top: ($("#letter").height()*(h/160))+224-$("#form\\:grid").height()/(h/160)
	},2000,function(){
		//Animation completed
		
		$("#closedLetter").css("z-index",4);
		$("#img1").css("visibility","hidden");
		$("#img2").css("visibility","hidden");
		$('#form\\:grid').css("visibility","hidden");
		aktivScale = setInterval(reScale, 5);
		setTimeout(moveLetter,1200);
	});
	//aktivMove = setInterval(moveToImg, 5);
	
}

function moveToImg(){
	if(imgh > h)
		{
			position.top = position.top+1;
			h = h+1;
			draw();
		}
	else{
		$("#closedLetter").css("z-index",4);
		$("#img1").css("visibility","hidden");
		$("#img2").css("visibility","hidden");
		$('#form\\:grid').css("visibility","hidden");
		clearInterval(aktivMove);
		aktivScale = setInterval(reScale, 5);
		//$('#closedLetter').css("transform","none");
		setTimeout(moveLetter,1200);
	}
}

function reScale(){
	console.log(scaleL);
	if(scaleL.w >= 1.0 || scaleL.h >= 1.0)
		{
		if(scaleL.w >= 1.0)
			scaleL.w = scaleL.w-0.01;
		if(scaleL.h >= 1.0)
			scaleL.h = scaleL.h-0.01;
		$("#closedLetter").css("transform", "scale("+scaleL.w+", "+scaleL.h+")");
		}
	else
		clearInterval(aktivScale);
}

function draw(){
	var t = position.top+"px";
	$("#form\\:grid").css("top",t);
}

function moveLetter(){
	pos = getPositions("#img4");
	posl = getPositions("#img3");
	
	console.log(pos);
	console.log(posl);
	aktivMove = setInterval(moveToBox,1);

}

function moveToBox(){
	if(posl[1][0] > pos[1][0]-50)
		{
			posl[1][0] -= 1;
			$("#img3").css("top",posl[1][0]+"px");
		}
	else
		{
			if(posl[0][1] < pos[0][1]+50)
			{
				posl[0][0] += 1;
				posl[0][1] +=1;
				$("#img3").css("left",posl[0][0]+"px");
			}
			else{
				//console.log("stop");
				$("#mail_box_animation").css("z-index",6);
				$("#img4").css("visibility","hidden");
				$("#img5").css("visibility","hidden");
				$("#closedLetter").css("visibility","hidden");
				clearInterval(aktivMove);
				setTimeout(openBox, 1800);
			}
		}

	
}
var lbox;
var rbox;
function openBox(){
	$("#img4").css("visibility","visible");
	$("#img5").css("visibility","visible");
	$("#img6").css("visibility","hidden");
    //$('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("background","yellow","!important");
    //$('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("opacity","0.1","!important");
    //$('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("position","absolute");
    var t = (-1)*($(window).height()-$('#form\\:LastInsertedPeople_data').height()-posl[1][0])+140;
    var l = (-1)*($(window).width()-$('#form\\:LastInsertedPeople_data').width()-posl[0][0])+20;
    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("top",t+"px");
    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("left",l+"px");
    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("z-index","4");
    
    lbox = $('#form\\:LastInsertedPeople_data').children("tr").eq(1).children("td").eq(0).width();
    rbox = $('#form\\:LastInsertedPeople_data').children("tr").eq(1).children("td").eq(1).width();
    console.log("Use: "+defaultSize.use);
    if(defaultSize.use != 1){
    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(0).css("width",lbox+10+"px");
    $('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(1).css("width",10+"px");
    }
    else
    	{
    	console.log(defaultSize.td1);
    	console.log(defaultSize.td2);
    	$('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(0).css("width",defaultSize.td1+"px");
        $('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(1).css("width",defaultSize.td2+"px");
    	}
    //$('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(1).css("padding","4px","10px");
    setTimeout(moveToHistory("#form\\:LastInsertedPeople_data'",100,l), 800);
}

function moveToHistory(id,w){
	$('#form\\:LastInsertedPeople_data').children("tr").eq(0).animate({
	    left: "-=700",
	    	opacity: 1.0
	  }, 2000, function() {
	    // Animation complete.
		  
	  });
	  setTimeout(function(){
		  $('#form\\:LastInsertedPeople_data').children("tr").eq(0).children("td").eq(1).css("width",rbox+"px");
	  }, 1000);
	$('#form\\:LastInsertedPeople_data').children("tr").eq(0).animate({
	    top: (-1)*$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(0).height()-10+"px"
	  }, 2000, function() {
	    // Animation complete.
	  });
	$('#form\\:LastInsertedPeople_data').children("tr").eq(0).animate({
		left: "0px"
	  }, 2000, function() {
	    // Animation complete.
	  });
	setTimeout(function(){
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(0).css("position","absolute");
		if(defaultSize.use != 1)
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(0).css("width",lbox+"px");
		else
			$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(0).css("width",defaultSize.td1+"px");
		
		var si = ($('#form\\:LastInsertedPeople_data').children("tr").length-1)*$('#form\\:LastInsertedPeople_data').children("tr").eq(0).height()+4;
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(0).css("bottom",(si)+"px");
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(1).css("position","absolute");
		if(defaultSize.use != 1){
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(1).css("width",rbox+"px");
		}else{
			$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(1).css("width",defaultSize.td2+"px");
		}
		console.log(si);
		console.log(defaultSize.size);
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(1).css("bottom",si+"px");
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(1).css("right","0px");
		
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(0).animate({
		    //bottom: (5*$('#form\\:LastInsertedPeople_data').children("tr").eq(0).height()+2)+"px",
		    bottom: "+="+$('#form\\:LastInsertedPeople_data').children("tr").eq(0).height()+"px"
		  }, 2000, function() {});
		
		
		$('#form\\:LastInsertedPeople_head').children("tr").eq(0).children("th").eq(1).animate({
		    //bottom: (5*$('#form\\:LastInsertedPeople_data').children("tr").eq(0).height()+2)+"px",
			bottom: "+="+$('#form\\:LastInsertedPeople_data').children("tr").eq(0).height()+"px"
		    
		  }, 2000, function() {
		    // Animation complete.
			  $('#form\\:LastInsertedPeople_data').children("tr").eq(0).css("background","grey","!important");
			  setTimeout(PF('ende').show(), 2200);
			  
			  }
		  
		);
	}, 4000);
	
	
}

function getPositions( elem ) {
        var pos, width, height;
        pos = $( elem ).position();
        width = $( elem ).width();
        height = $( elem ).height();
        return [ [ pos.left, pos.left + width ], [ pos.top, pos.top + height ] ];
    }


